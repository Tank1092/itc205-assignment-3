package library;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import library.interfaces.EBorrowState;
import library.interfaces.IBorrowUI;
import library.interfaces.IBorrowUIListener;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.interfaces.hardware.ICardReader;
import library.interfaces.hardware.ICardReaderListener;
import library.interfaces.hardware.IDisplay;
import library.interfaces.hardware.IPrinter;
import library.interfaces.hardware.IScanner;
import library.interfaces.hardware.IScannerListener;

public class BorrowUC_CTL implements ICardReaderListener, 
									 IScannerListener, 
									 IBorrowUIListener {
	
	private ICardReader reader_;
	private IScanner scanner_; 
	private IPrinter printer_; 
	private IDisplay display_;
	private int scanCount_ = 0;
	private IBorrowUI ui_;
	private EBorrowState state_; 
	private IBookDAO bookDAO_;
	private IMemberDAO memberDAO_;
	private ILoanDAO loanDAO_;
	private final int MAX_LOANS_ = IMember.LOAN_LIMIT;
	
	private List<IBook> bookList_;
	private List<ILoan> loanList_;
	private IMember borrower_;
	
	private JPanel previous_;


	public BorrowUC_CTL(ICardReader reader, IScanner scanner, 
			IPrinter printer, IDisplay display,
			IBookDAO bookDAO, ILoanDAO loanDAO, IMemberDAO memberDAO ) {

        reader_ = reader;
        scanner_ = scanner;
        printer_ = printer;
        display_ = display; 
        ui_ = new BorrowUC_UI(this);
        bookDAO_ = bookDAO;
        memberDAO_ = memberDAO;
        loanDAO_ = loanDAO;
        
        bookList_ = new ArrayList<IBook>();
        loanList_ = new ArrayList<ILoan>();
        
        reader_.addListener(this);
        scanner_.addListener(this);
	}
	
	
	
	public void initialise() {
		previous_ = display_.getDisplay();
		display_.setDisplay((JPanel) ui_, "Borrow UI");	
		setState(EBorrowState.INITIALIZED);
	}
	
	
	
	public void close() {
		display_.setDisplay(previous_, "Main Menu");
	}

	
	
	@Override
	public void cardSwiped(int memberID) throws RuntimeException {
        if (state_ != EBorrowState.INITIALIZED) {
            throw new RuntimeException("BorrowUC_CTL: cardSwiped - Controller in incorrect state. Should be: INITIALIZED, but was: " + state_.name());
        }
        
        borrower_ = memberDAO_.getMemberByID(memberID);
        
        if (borrower_ == null){
            ui_.displayErrorMessage("Member not found!");
            return;
        }
        
        boolean overdue = borrower_.hasOverDueLoans();
        boolean atLoanLimit = borrower_.hasReachedLoanLimit();
        boolean hasFines = borrower_.hasFinesPayable();
        boolean overFineLimit = borrower_.hasReachedFineLimit();
        
        String loanDetails = "";
        float amountOwing;
        
        if (overdue || atLoanLimit || overFineLimit){
            setState(EBorrowState.BORROWING_RESTRICTED);
            ui_.displayErrorMessage("Borrowing restricted");
        } 
        else {
            setState(EBorrowState.SCANNING_BOOKS);
        }
        
        scanCount_ = borrower_.getLoans().size();
     
        if (hasFines){
            amountOwing = borrower_.getFineAmount();
            ui_.displayOutstandingFineMessage(amountOwing);
        }
        
        if (overdue){
            ui_.displayOverDueMessage();
        }
        
        if (atLoanLimit){
            ui_.displayAtLoanLimitMessage();
        }
        
        if (overFineLimit){
            amountOwing = borrower_.getFineAmount();
            ui_.displayOverFineLimitMessage(amountOwing);
        }

        String memberName = borrower_.getFirstName() + " " + borrower_.getLastName();
        String memberPhone = borrower_.getContactPhone();
        ui_.displayMemberDetails(memberID, memberName, memberPhone);
        
        loanDetails = buildLoanListDisplay(borrower_.getLoans()); 
        ui_.displayExistingLoan(loanDetails);
	}
	
	
	
	@Override
	public void bookScanned(int barcode) throws RuntimeException {
        if (state_ != EBorrowState.SCANNING_BOOKS){
            throw new RuntimeException("BorrowUC_CTL: bookScanned - Controller in incorrect state. Should be: SCANNING_BOOKS, but was: " + state_.name());
        }
        
        IBook book = bookDAO_.getBookByID(barcode);
        
        ui_.displayErrorMessage("");
        
        if (book == null){
            ui_.displayErrorMessage("Book not found");
            return;
        }
        
        EBookState bookState = book.getState();
        
        if (bookState != EBookState.AVAILABLE){
            ui_.displayErrorMessage("Book not available");
            return;
        }
        
        if (bookList_.contains(book)){
            ui_.displayErrorMessage("Book already scanned");
            return;
        } 
        
        scanCount_++;
        
        bookList_.add(book);
        ILoan loan = loanDAO_.createLoan(borrower_, book);
        loanList_.add(loan);

        String bookDetails = book.toString();
        String loanDetails = buildLoanListDisplay(loanList_);

        ui_.displayScannedBookDetails(bookDetails);
        ui_.displayPendingLoan(loanDetails);
        
        if (scanCount_ >= MAX_LOANS_) {
            scansCompleted();
        }
	}

	
	// This method is not private to facilitate testing
	protected void setState(EBorrowState state) throws RuntimeException {
	    state_ = state;
	    ui_.setState(state);
        
        switch (state) {

        case INITIALIZED:
            reader_.setEnabled(true);
            scanner_.setEnabled(false);
            break;

        case SCANNING_BOOKS:
            reader_.setEnabled(false);
            scanner_.setEnabled(true);
            break;

        case BORROWING_RESTRICTED:
            reader_.setEnabled(false);
            scanner_.setEnabled(false);
            break;

        case CONFIRMING_LOANS:
            reader_.setEnabled(false);
            scanner_.setEnabled(false);
            break;

        case COMPLETED:
            reader_.setEnabled(false);
            scanner_.setEnabled(false);
            break;

        case CANCELLED:
            reader_.setEnabled(false);
            scanner_.setEnabled(false);
            break;

        default:
            throw new RuntimeException("Attempted to set BorrowUC_CTL to unanticipated state");
        }
	}

	
	
	@Override
	public void cancelled() {
	    setState(EBorrowState.CANCELLED);
	    
	    loanList_.clear();
        bookList_.clear();
        
		close();
	}
	
	
	
	@Override
	public void scansCompleted() throws RuntimeException {
        if (state_ != EBorrowState.SCANNING_BOOKS){
            throw new RuntimeException("BorrowUC_CTL: scansCompleted - Controller in incorrect state. Should be: SCANNING_BOOKS, but was: " + state_.name());
        }
        
        setState(EBorrowState.CONFIRMING_LOANS);
        
        String loanDetails = buildLoanListDisplay(loanList_);
        
        ui_.displayConfirmingLoan(loanDetails);
	}

	
	
	@Override
	public void loansConfirmed() throws RuntimeException {
        if (state_ != EBorrowState.CONFIRMING_LOANS){
            throw new RuntimeException("BorrowUC_CTL: loansConfirmed - Controller in incorrect state. Should be: CONFIRMING_LOANS, but was: " + state_.name());
        }
        
        for (ILoan loan : loanList_){
                loanDAO_.commitLoan(loan);
        }
        
        String loansDetails = buildLoanListDisplay(loanList_) + " "; // Added space in case the list is empty to stop exception in printer
        
        printer_.print(loansDetails);
        
        setState(EBorrowState.COMPLETED);
        
        close();
	}

	
	
	@Override
	public void loansRejected() throws RuntimeException {
        if (state_ != EBorrowState.CONFIRMING_LOANS){
            throw new RuntimeException("BorrowUC_CTL: loansRejected - Controller in incorrect state. Should be: CONFIRMING_LOANS, but was: " + state_.name());
        }
        
        loanList_.clear();
        bookList_.clear();

        setState(EBorrowState.SCANNING_BOOKS);
        
        ui_.displayScannedBookDetails("");
        ui_.displayPendingLoan("");
        
        scanCount_ = borrower_.getLoans().size();
	}

	
	
	private String buildLoanListDisplay(List<ILoan> loans) {
		StringBuilder bld = new StringBuilder();
		for (ILoan ln : loans) {
			if (bld.length() > 0) bld.append("\n\n");
			bld.append(ln.toString());
		}
		return bld.toString();		
	}
	
	
	
	/* Methods below this line are to facilitate testing */
	
	
	
	EBorrowState getState(){
	    return state_;
	}
	
	
	
	int getScanCount(){
	    return scanCount_;
	}
	
	
	
    void setScanCount(int scanCount) {
        scanCount_ = scanCount;
    }
}
