package library.entities;

import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.EBookState;

public class Book implements IBook {
    private String author_;
    private String title_;
    private String callNumber_;
    private int id_;
    private ILoan loan_;
    private EBookState state_;
    
    
    public Book (String author, String title, String callNumber, int bookID) throws IllegalArgumentException {
        sane(author, title, callNumber, bookID);
        
        author_ = author;
        title_ = title;
        callNumber_ = callNumber;
        id_ = bookID;
        loan_ = null;
        state_ = EBookState.AVAILABLE;
    }
    
    

    private void sane (String author, String title, String callNumber, int bookID) throws IllegalArgumentException {
        boolean isAuthorNull = author == null;
        boolean isTitleNull = title == null;
        boolean isCallNumberNull = callNumber == null;
        
        boolean isAuthorBlank;
        boolean isTitleBlank;
        boolean isCallNumberBlank;
        
        if (isAuthorNull || isTitleNull || isCallNumberNull){
            throw new IllegalArgumentException("Book: constructor - Parameter was null");
        }
        
        isAuthorBlank = author.trim().length() == 0;
        isTitleBlank = title.trim().length() == 0;
        isCallNumberBlank = callNumber.trim().length() == 0;
        
        if (isAuthorBlank || isTitleBlank || isCallNumberBlank){
            throw new IllegalArgumentException("Book: constructor - Parameter was blank");
        }
        
        if (bookID <= 0){
            throw new IllegalArgumentException("Book: constructor - BookID is not positive");
        }
    }
    
    
    
    @Override
    public void borrow (ILoan loan) throws RuntimeException{
        boolean available = state_ == EBookState.AVAILABLE;
        
        if (!available){
            throw new RuntimeException("Book: borrow - Book is not available");
        }
        
        loan_ = loan;
        state_ = EBookState.ON_LOAN;
    }
    
    
    
    @Override
    public ILoan getLoan(){
        boolean onLoan = state_ == EBookState.ON_LOAN;
        
        if (!onLoan){
            return null;
        }
        
        return loan_;    
    }
    
    
    
    @Override
    public void returnBook (boolean damaged) throws RuntimeException{
        boolean onLoan = state_ == EBookState.ON_LOAN;
        
        if (!onLoan){
            throw new RuntimeException("Book: returnBook - Book is not currently on loan");
        }
        
        if (damaged){
            state_ = EBookState.DAMAGED;
        } else {
            state_ = EBookState.AVAILABLE;
        }
        
        loan_ = null;
    }
    
    
    
    @Override
    public void lose() throws RuntimeException{
        boolean onLoan = state_ == EBookState.ON_LOAN;
        
        if (!onLoan){
            throw new RuntimeException("Book: lose - Book is not currently on loan");
        }
        
        state_ = EBookState.LOST;
    }
    
    
    
    @Override
    public void repair() throws RuntimeException {
        boolean damaged = state_ == EBookState.DAMAGED;
        
        if (!damaged){
            throw new RuntimeException("Book: repair - Book is not damaged");
        }
        
        state_ = EBookState.AVAILABLE;
    }
    
    
    
    @Override
    public void dispose() throws RuntimeException {
        boolean isOnloan = state_ == EBookState.ON_LOAN;

        if (isOnloan){
            throw new RuntimeException("Book: dispose - Book cannot be disposed");
        }

        state_ = EBookState.DISPOSED;
    }
    
    
    
    @Override
    public EBookState getState(){
        return state_;
    }
    
    
    
    @Override
    public String getAuthor(){
        return author_;
    }
    
    
    
    @Override
    public String getTitle(){
        return title_;
    }
    
    
    
    @Override
    public String getCallNumber(){
        return callNumber_;
    }
    
    
    
    @Override
    public int getID(){
        return id_;
    }
 
    
    
    public String toString(){
        return String.format("Id: %d \nTitle: %s \nAuthor: %s \nCall Number: %s", 
                             id_, title_, author_, callNumber_);
    }
    
    
    /* Methods below this line are here to facilitate testing */
    
    void setState(EBookState state){
        state_ = state;
    }
}
