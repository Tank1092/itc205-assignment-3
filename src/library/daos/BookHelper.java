package library.daos;

import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;
import library.entities.Book;

public class BookHelper implements IBookHelper{

    @Override
    public IBook makeBook(String author, String title, String callNumber, int id) throws IllegalArgumentException {

        return new Book(author, title, callNumber, id);
    }

}
