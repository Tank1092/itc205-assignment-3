package library.daos;

import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class BookMapDAO implements IBookDAO {
    private int nextId_;
    private HashMap<Integer, IBook> bookMap_;
    private IBookHelper helper_;

    
    
    public BookMapDAO (IBookHelper helper) throws IllegalArgumentException {
        if (helper == null){
            throw new IllegalArgumentException("BookMpDAO: constructor - Helper object is null");
        }
        
        nextId_ = 1;
        bookMap_ = new HashMap<>();
        helper_ = helper;
    }
    
    
    
    @Override
    public IBook addBook (String author, String title, String callNo) {
        IBook book = null;
        try {
            book = helper_.makeBook(author, title, callNo, nextId_);
        } catch (IllegalArgumentException e){
            return null;
        }
        
        bookMap_.put(nextId_, book);
        nextId_++;
        
        return book;
    }

    
    
    @Override
    public IBook getBookByID (int id) {
        IBook book = bookMap_.get(id);

        return book;
    }

    
    
    @Override
    public List<IBook> listBooks() {
        List<IBook> bookList = new ArrayList<IBook>(bookMap_.values());
       
        return bookList;
    }

    
    
    @Override
    public List<IBook> findBooksByAuthor (String author) throws IllegalArgumentException {
        if (author == null){
            throw new IllegalArgumentException("BookMpDAO: findBooksByAuthor - Author must not be null");
        }
        
        List<IBook> bookList = listBooks();
        List<IBook> matchedBookList = new ArrayList<>();
        
        for (IBook book : bookList){
            if (book.getAuthor().contains(author)){
                matchedBookList.add(book);
            } 
        }
        
        return matchedBookList;
    }

    
    
    @Override 
    public List<IBook> findBooksByTitle (String title) throws IllegalArgumentException {   
        if (title == null){
            throw new IllegalArgumentException("BookMpDAO: findBooksByTitle - Title must not be null");
        }
        
        List<IBook> bookList = listBooks();
        List<IBook> matchedBookList = new ArrayList<>();
        
        for (IBook book : bookList){
            if (book.getTitle().contains(title)){
                matchedBookList.add(book);
            } 
        }
        
        return matchedBookList;
    }

    
    
    @Override
    public List<IBook> findBooksByAuthorTitle (String author, String title) throws IllegalArgumentException {
        if (title == null || author == null){
            throw new IllegalArgumentException("BookMpDAO: findBooksByAuthorTitle - Author or Title must not be null");
        }
        
        
        List<IBook> bookList = listBooks();
        List<IBook> matchedBookList = new ArrayList<>();
        
        for (IBook book : bookList){
            if (book.getAuthor().contains(author) && book.getTitle().contains(title)){
                matchedBookList.add(book);
            } 
        }
        
        return matchedBookList;
    }
}
