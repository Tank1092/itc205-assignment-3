package library;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import javax.swing.JPanel;

import library.daos.BookMapDAO;
import library.daos.BookHelper;
import library.daos.LoanMapDAO;
import library.daos.LoanHelper;
import library.daos.MemberMapDAO;
import library.daos.MemberHelper;

import library.interfaces.EBorrowState;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.IBookHelper;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.ILoanHelper;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.daos.IMemberHelper;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.interfaces.hardware.ICardReader;
import library.interfaces.hardware.IDisplay;
import library.interfaces.hardware.IPrinter;
import library.interfaces.hardware.IScanner;
import library.hardware.CardReader;
import library.hardware.Scanner;
import library.hardware.Printer;
import library.hardware.Display;

import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

// BorrowUC_CTL complete integration test for testing all system operations
@RunWith(MockitoJUnitRunner.class)
public class BorrowUC_CTLFullIntegrationTest {

    @Spy
    private ICardReader reader_ = new CardReader();
    
    @Spy
    private IScanner scanner_ = new Scanner();
    
    @Spy
    private IPrinter printer_ = new Printer();
    
    @Spy
    private IDisplay display_ = new Display();
    
    private IBookHelper bookHelper_ = new BookHelper();
    
    private ILoanHelper loanHelper_ = new LoanHelper();
    
    private IMemberHelper memberHelper_ = new MemberHelper();
    
    @Spy
    private IBookDAO bookDAO_ = new BookMapDAO(bookHelper_);
    
    @Spy
    private ILoanDAO loanDAO_ = new LoanMapDAO(loanHelper_); 
    
    @Spy
    private IMemberDAO memberDAO_ = new MemberMapDAO(memberHelper_);
     
    private IBook book1_, book2_, book3_, book4_, book5_, book6_, book7_, book8_;
    
    private ILoan loan1_, loan2_, loan3_, loan4_, loan5_, loan6_, loan7_;
 
    private IMember member1_, member2_, member3_, member4_, member5_;
    
                                // The member added to the map here is the original, NOT the spy
                                // Used for tests which require the CTL to have a borrower as not null
    @Spy                        // Also used to inject loans into
    private IMember borrower_ = spy(memberDAO_.addMember("Jim", "Jimson", "1111-222-333", "Jim@Jimson.com")); 
    
    @Spy
    private List<IBook> bookList_ = new ArrayList<IBook>();
    
    @Spy
    private List<ILoan> loanList_ = new ArrayList<ILoan>();
    
    @Spy
    private JPanel previous_ = display_.getDisplay();
    
    @InjectMocks
    private BorrowUC_CTL sut_ = new BorrowUC_CTL(reader_, scanner_, printer_, display_,
                                                 bookDAO_, loanDAO_, memberDAO_);
    
    @Spy 
    private BorrowUC_UI ui_ = new BorrowUC_UI(sut_); 
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    
    
    @Before
    public void setUp() throws Exception {
        // Setup the books
        book1_ = bookDAO_.addBook("Bob Bobson", "Tales of Jim Jimson", "12334567890"); // Not on loan
        book2_ = bookDAO_.addBook("Bob Bobson", "The Secret Life of Jim Jimson", "5932871");
        book3_ = bookDAO_.addBook("Jim Jimson", "The Shadow: My encounter with a stalker", "00000000");
        book4_ = bookDAO_.addBook("Anon", "The fascinating story of pizza and cheese part 1", "00000001");
        book5_ = bookDAO_.addBook("Anon", "The fascinating story of pizza and cheese part 2", "00000002");
        book6_ = bookDAO_.addBook("Richard Richardson", "I think I lost my toothbrush", "16765472");
        book7_ = bookDAO_.addBook("Pamela Johnson", "A walk in the dark", "1564343");
        book8_ = bookDAO_.addBook("Kelly Richardson", "Some other nonsense title", "12315");
        
        // Setup the members
        member1_ = borrower_;                                                                               // No loans   
        member2_ = memberDAO_.addMember("Bob", "Bobson", "4444-555-666", "Bob@Bobson.com");                 // Overdue loan
        member3_ = memberDAO_.addMember("Richard", "Richardson", "7777-888-999", "Richard@Richardson.com"); // Has fines but under limit
        member4_ = memberDAO_.addMember("Kelly", "Richardson", "1234-567-890", "Kelly@Richardson.com");     // At maximum fine limit
        member5_ = memberDAO_.addMember("Pamela", "Johnson", "0000-000-000", "Pam@Bigpond.com.au");         // At loan limit
        
        // Force the DAO to return the spy, instead of the original
        when(memberDAO_.getMemberByID(1)).thenReturn(member1_); 
        
        loan1_ = loanDAO_.createLoan(member2_, book3_);    // Overdue
        loan2_ = loanDAO_.createLoan(member3_, book5_);    
        
        // Put member 5 over the loan limit
        loan3_ = loanDAO_.createLoan(member5_, book2_);
        loan4_ = loanDAO_.createLoan(member5_, book4_);
        loan5_ = loanDAO_.createLoan(member5_, book6_);
        loan6_ = loanDAO_.createLoan(member5_, book7_);
        loan7_ = loanDAO_.createLoan(member5_, book8_); 
        
        // Code from Main.java - Thanks Jim!
        // Make member2's loan be overdue
        Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();
        
        loanDAO_.commitLoan(loan1_);
        cal.setTime(now);
        cal.add(Calendar.DATE, ILoan.LOAN_PERIOD + 1);
        Date checkDate = cal.getTime();     
        loanDAO_.updateOverDueStatus(checkDate);
        
        loanDAO_.commitLoan(loan2_);
        loanDAO_.commitLoan(loan3_);
        loanDAO_.commitLoan(loan4_);
        loanDAO_.commitLoan(loan5_);
        loanDAO_.commitLoan(loan6_);
        loanDAO_.commitLoan(loan7_);
        
        // Give member 3 a small fine
        memberDAO_.getMemberByID(3).addFine(3.0f);
        
        // Give member 4 the max fine
        memberDAO_.getMemberByID(4).addFine(10.0f);
    }

    
    
    @After
    public void tearDown() throws Exception {
        // Everything is made null here to make it clear that nothing should 'carry over' between tests
        reader_ = null;
        scanner_ = null;
        printer_ = null;
        display_ = null;
        
        bookHelper_ = null;
        loanHelper_ = null;
        memberHelper_ = null;
        
        bookDAO_ = null;
        loanDAO_ = null;
        memberDAO_ = null;
        
        book1_ = null;  
        book2_ = null;
        book3_ = null;
        book4_ = null;  
        book5_ = null;
        book6_ = null;
        book7_ = null;
        book8_ = null;
        
        loan1_ = null;
        loan2_ = null;
        loan3_ = null;
        loan4_ = null;
        loan5_ = null;
        loan6_ = null;
        loan7_ = null;
        
        member1_ = null;
        member2_ = null;
        member3_ = null;
        member4_ = null;
        member5_ = null;
        
        borrower_ = null;
        bookList_ = null;
        loanList_ = null;
        previous_ = null;
        
        sut_ = null;
        ui_ = null;
    }

    /// ******* INITIALISATION TESTS ******* ///
    
    @Test
    public void initialisation_ShouldDisplay_BorrowingUI() throws Exception {
        sut_.initialise();
        
        verify(display_).setDisplay(ui_, "Borrow UI");
    }
    
    
    
    @Test
    public void initialisation_ShouldEnable_CardReader() throws Exception {
        sut_.initialise();
        
        verify(reader_).setEnabled(true);
    }
    
    
    
    @Test
    public void initialisation_ShouldDisable_Scanner() throws Exception {
        sut_.initialise();
        
        verify(scanner_).setEnabled(false);
    }
    
    
    
    @Test
    public void initialisation_ShouldSetCTLStateTo_INITIALIZED() throws Exception {
        EBorrowState expected = EBorrowState.INITIALIZED;
        
        sut_.initialise();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test
    public void initialisation_ShouldSetUIStateTo_INITIALIZED() throws Exception {
        EBorrowState expected = EBorrowState.INITIALIZED;
        
        sut_.initialise();
        
        verify(ui_).setState(expected);
    }
    
    
    /// ******* CARD SWIPING TESTS ******* ///
    
    
    @Test
    public void swipingCard_ShouldThrowExceptionWhen_StateNot_INITIALIZED() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        final int ARBITRARY_ID = 1;
        
        sut_.cardSwiped(ARBITRARY_ID);
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test
    public void swipingCard_ShouldDisplay_ErrorMessageWhen_MemberDoesNotExist() throws Exception {
        final int ARBITRARY_ID = 999;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(ARBITRARY_ID);
        
        verify(ui_).displayErrorMessage(contains("not found"));
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisable_CardReader() throws Exception {
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(reader_).setEnabled(false);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisable_Scanner() throws Exception {
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(scanner_).setEnabled(false);
    }



    @Test 
    public void swipingCard_ShouldSetCTLStateTo_SCANNING_BOOKS() throws Exception {
        final int MEMBER_ID = 1;
        EBorrowState expected = EBorrowState.SCANNING_BOOKS;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void swipingCard_ShouldSetCTLStateTo_BORROWING_RESTRICTED_WhenMemberRestricted() throws Exception {
        final int MEMBER_ID = 2;
        EBorrowState expected = EBorrowState.BORROWING_RESTRICTED;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void swipingCard_ShouldSetUIStateTo_SCANNING_BOOKS() throws Exception {
        final int MEMBER_ID = 1;
        EBorrowState expected = EBorrowState.SCANNING_BOOKS;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).setState(expected);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldSetUIStateTo_BORROWING_RESTRICTED_WhenMemberRestricted() throws Exception {
        final int MEMBER_ID = 2;
        EBorrowState expected = EBorrowState.BORROWING_RESTRICTED;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).setState(expected);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldSetScanCountTo_NumberOfExistingLoans() throws Exception {
        final int MEMBER_ID = 3;
        int expected = member3_.getLoans().size();
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        int actual = sut_.getScanCount();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_Member1Details() throws Exception {
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayMemberDetails(MEMBER_ID, "Jim Jimson", "1111-222-333");
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_ExistingLoans() throws Exception {
        final int MEMBER_ID = 2;
        String expected = loan1_.toString();
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayExistingLoan(expected);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_NoExistingLoanDetailsWhen_ExistingLoanListEmpty() throws Exception {
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayExistingLoan("");
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_OutstandingFineMessageWhen_MemberHasFines() throws Exception {
        final int MEMBER_ID = 4;
        float expectedAmount = memberDAO_.getMemberByID(MEMBER_ID).getFineAmount();
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayOutstandingFineMessage(expectedAmount);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_OverdueMessageWhen_MemberRestrictedAndHasOverdueBooks() throws Exception {
        final int MEMBER_ID = 2;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayOverDueMessage();
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_AtLoanLimitMessageWhen_MemberRestrictedAndAtLoanLimit() throws Exception {
        final int MEMBER_ID = 5;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayAtLoanLimitMessage();
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_OverFineLimitMessageWhen_MemberRestrictedAndOverFineLimit() throws Exception {
        final int MEMBER_ID = 4;
        float expectedAmount = memberDAO_.getMemberByID(MEMBER_ID).getFineAmount();
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayOverFineLimitMessage(expectedAmount);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_BorrowingRestrictedMessageWhen_MemberRestricted() throws Exception {
        final int MEMBER_ID = 5;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayErrorMessage(contains("Borrowing restricted"));
    }
    
    
    /// ******* BOOK SCANNING TESTS ******* ///
    
    
    @Test 
    public void scanningBook_ShouldThrowExceptionWhen_StateNot_SCANNING_BOOKS() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.INITIALIZED);
        final int ARBITRARY_ID = 1;
        
        sut_.bookScanned(ARBITRARY_ID);
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test 
    public void scanningBook_ShouldIncrement_ScanCountByOne() throws Exception {
        int expected = 4;
        sut_.setScanCount(expected - 1);
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        int actual = sut_.getScanCount();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisplay_ConfirmLoanUI_AfterLoanLimitReached() throws Exception {
        sut_.setScanCount(4);
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        EBorrowState expected = EBorrowState.CONFIRMING_LOANS;
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).setState(expected);
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisable_CardReader() throws Exception {
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(reader_).setEnabled(false);
    }
    
    
    
    @Test 
    public void scanningBook_ShouldEnable_Scanner() throws Exception {
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(scanner_).setEnabled(true);
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisplay_ErrorMessageWhen_BookNotFound() throws Exception {
        final int BOOK_ID = 999;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).displayErrorMessage(contains("not found"));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisplay_ErrorMessageWhen_BookNotAvailable() throws Exception {
        final int BOOK_ID = 2;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).displayErrorMessage(contains("not available"));
    }

    
    
    @Test 
    public void scanningBook_ShouldDisplay_ErrorMessageWhen_BookAlreadyScanned() throws Exception {
        final int BOOK_ID = 1;
        bookList_.add(book1_);
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).displayErrorMessage(contains("already scanned"));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldAddBookTo_ScannedBookList() throws Exception {
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        assertThat(bookList_, hasItem(book1_));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldCreate_NewPendingLoan() throws Exception {
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        IBook expectedBook = book1_;  
        IMember expectedBorrower = borrower_;
        
        sut_.bookScanned(BOOK_ID);
        
        assertThat(loanList_.get(0).getBook(), equalTo(expectedBook));
        assertThat(loanList_.get(0).getBorrower(), equalTo(expectedBorrower));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisplay_PendingLoanList() throws Exception {
        String expectedBorrower = "Borrower: " + borrower_.getFirstName() + " " + borrower_.getLastName();
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).displayPendingLoan(contains(expectedBorrower));
    }
    
    
    /// ******* SCAN COMPLETION TESTS ******* ///
    
    
    @Test 
    public void completingScan_ShouldThrowExceptionWhen_StateNot_SCANNING_BOOKS() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.scansCompleted();
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test 
    public void completingScan_ShouldDisplay_ConfirmLoanUI() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        EBorrowState expected = EBorrowState.CONFIRMING_LOANS;
        
        sut_.scansCompleted();
        
        verify(ui_).setState(expected);
    }
    
    
    
    @Test 
    public void completingScan_ShouldDisplay_PendingLoanList() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        loanList_.add(loan1_);
        loanList_.add(loan2_);
        String expected = loan1_ + "\n\n" + loan2_;
        
        sut_.scansCompleted();
        
        verify(ui_).displayConfirmingLoan(expected);
    }
    
    
    
    @Test 
    public void completingScan_ShouldDisable_CardReader() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.scansCompleted();
        
        // atMost quantifier needed because of earlier setState call
        verify(reader_, atMost(2)).setEnabled(false); 
    }
    
    
    
    @Test 
    public void completingScan_ShouldDisable_Scanner() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.scansCompleted();
        
        verify(scanner_).setEnabled(false);
    }
    
    
    
    @Test 
    public void completingScan_ShouldSetCTLStateTo_CONFIRMING_LOANS() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        EBorrowState expected = EBorrowState.CONFIRMING_LOANS;
        
        sut_.scansCompleted();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    /// ******* LOAN REJECTION TESTS ******* ///
    
    
    @Test 
    public void rejectingLoans_ShouldThrowExceptionWhen_StateNot_CONFIRMING_LOANS() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.loansRejected();
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldDisplay_BorrowingUI() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        EBorrowState expected = EBorrowState.SCANNING_BOOKS;
        
        sut_.loansRejected();
        
        verify(ui_).setState(expected);
    }

    
    
    @Test 
    public void rejectingLoans_ShouldClear_PendingLoanList() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        final int ZERO = 0;
        loanList_.add(loan1_);
        
        sut_.loansRejected();
        
        assertThat(loanList_.size(), equalTo(ZERO));
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldClear_ScannedBookList() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        final int ZERO = 0;
        bookList_.add(book1_);
        
        sut_.loansRejected();
        
        assertThat(bookList_.size(), equalTo(ZERO));
    }
        
    
    
    @Test 
    public void rejectingLoans_ShouldSetScanCountTo_NumberOfExistingLoans() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        int expected = borrower_.getLoans().size();
        sut_.setScanCount(expected + 1);
        
        sut_.loansRejected();
        int actual = sut_.getScanCount();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldDisable_CardReader() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansRejected();
        
        // atMost quantifier needed because of earlier setState call
        verify(reader_, atMost(2)).setEnabled(false);
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldEnable_Scanner() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansRejected();
        
        // atMost quantifier needed because of earlier setState call
        verify(scanner_, atMost(2)).setEnabled(true);
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldSetCTLStateTo_SCANNING_BOOKS() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        EBorrowState expected = EBorrowState.SCANNING_BOOKS;
        
        sut_.loansRejected();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    /// ******* LOAN CONFIRMATION TESTS ******* ///
    
    
    @Test 
    public void confirmingLoans_ShouldThrowExceptionWhen_StateNot_CONFIRMING_LOANS() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.loansConfirmed();
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldCommit_PendingLoans() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        ILoan newLoan = loanDAO_.createLoan(member1_, book1_);
        loanList_.add(newLoan);
        
        sut_.loansConfirmed();
        
        verify(loanDAO_).commitLoan(newLoan);
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldDisplay_MainMenu() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansConfirmed();
        
        verify(display_).setDisplay(anyObject(), eq("Main Menu"));
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldDisplay_ListOfCommittedLoans() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        ILoan newLoan = loanDAO_.createLoan(member1_, book1_);
        loanList_.add(newLoan);
        
        String expectedBorrower = newLoan.getBorrower().getFirstName() + " " + newLoan.getBorrower().getLastName();
        String expectedBoook = newLoan.getBook().getTitle();
        
        sut_.loansConfirmed();
        
        verify(printer_).print(contains(expectedBorrower));
        verify(printer_).print(contains(expectedBoook));
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldDisable_CardReader() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansConfirmed();
        
        // atMost quantifier needed because of earlier setState call
        verify(reader_, atMost(2)).setEnabled(false);
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldDisable_Scanner() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansConfirmed();
        
        // atMost quantifier needed because of earlier setState call
        verify(scanner_, atMost(2)).setEnabled(false);
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldSetCTLStateTo_COMPLETED() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        EBorrowState expected = EBorrowState.COMPLETED;
        
        sut_.loansConfirmed();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    /// ******* CANCEL OPERATION TESTS ******* ///
    
    
    @Test 
    public void cancelling_ShouldDisplay_MainMenu() throws Exception {
        sut_.cancelled();
        
        verify(display_).setDisplay(anyObject(), eq("Main Menu"));
    }
    
    
    
    @Test 
    public void cancelling_ShouldDisable_Reader() throws Exception {
        sut_.cancelled();
        
        verify(reader_).setEnabled(false);
    }
    
    
    
    @Test 
    public void cancelling_ShouldDisable_Scanner() throws Exception {
        sut_.cancelled();
        
        verify(scanner_).setEnabled(false);
    }
    
    
    
    @Test 
    public void cancelling_ShouldSetCTLStateTo_CANCELLED() throws Exception {
        sut_.setState(EBorrowState.COMPLETED);
        EBorrowState expected = EBorrowState.CANCELLED;
        
        sut_.cancelled();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
}
