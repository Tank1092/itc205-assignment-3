package library.daos;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import library.daos.BookMapDAO;
import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.LinkedList;
import java.util.HashMap;

// BookMapDAO integration test with BookHelper
@RunWith(MockitoJUnitRunner.class)
public class BookMapDAOBookHelperIntegrationTest {
    
    @Spy
    private HashMap<Integer, IBook> bookMap_;
    
    @Spy
    private IBookHelper helper_ = new BookHelper();
    
    @Mock 
    private IBook mockBook1_, mockBook2_, mockBook3_;
    
    @InjectMocks
    private BookMapDAO sut_ = new BookMapDAO(helper_);
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    
    
    @Before
    public void setUp() throws Exception {
        // Setup the helper and mocked books
        doReturn(mockBook1_).when(helper_).makeBook(eq("Bob Bobson"), eq("Tales of Jim Jimson"), eq("12334567890"), anyInt());
        doReturn(mockBook2_).when(helper_).makeBook(eq("Bob Bobson"), eq("The Secret Life of Jim Jimson"), eq("5932871"), anyInt());
        doReturn(mockBook3_).when(helper_).makeBook(eq("Jim Jimson"), eq("The Shadow: My encounter with a stalker"), eq("00000000"), anyInt());
        
        when(mockBook1_.getAuthor()).thenReturn("Bob Bobson");
        when(mockBook2_.getAuthor()).thenReturn("Bob Bobson");
        when(mockBook3_.getAuthor()).thenReturn("Jim Jimson"); 
        
        when(mockBook1_.getTitle()).thenReturn("Tales of Jim Jimson");
        when(mockBook2_.getTitle()).thenReturn("The Secret Life of Jim Jimson");
        when(mockBook3_.getTitle()).thenReturn("The Shadow: My encounter with a stalker");
    }

    
    
    @After
    public void tearDown() throws Exception {
        helper_ = null;
    }

    
    
    @Test
    public void instantiatingDAO_ShouldThrowExceptionWhen_HelperIsNull() {
        thrown.expect(IllegalArgumentException.class);
        
        new BookMapDAO(null);
        
        fail("Exception should have been thrown.");
    }

    
    
    @Test 
    public void addingBook_ShouldAddBookToMap() throws Exception {
        IBook expected = mockBook1_;
        final int BOOK_ID = 1;
        
        sut_.addBook("Bob Bobson", "Tales of Jim Jimson", "12334567890");
        IBook actual = bookMap_.get(BOOK_ID);
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void addingBook_ShouldNotAddBookToMapWhen_HelperThrowsException() throws Exception {
        doThrow(IllegalArgumentException.class).when(helper_).makeBook(anyString(), anyString(), anyString(), anyInt());
        final int BOOK_ID = 1;
        
        sut_.addBook(" ", "Tales of Jim Jimson", "12334567890");
        IBook actual = bookMap_.get(BOOK_ID);
        
        assertNull(actual);
    }
    
    
    
    @Test 
    public void listOfBooks_ShouldContain_NoBooks() throws Exception {
        List<IBook> expectedEmptyList = new LinkedList<IBook>();
        
        List<IBook> actualList = sut_.listBooks();
        
        assertThat(actualList, equalTo(expectedEmptyList));
    }
    
    
    
    @Test 
    public void listOfBooks_ShouldContain_MockBook1() throws Exception {
        List<IBook> expectedList = new LinkedList<IBook>();
        final int BOOK_ID = 1;
        expectedList.add(mockBook1_);
        bookMap_.put(BOOK_ID, mockBook1_);
        
        List<IBook> actualList = sut_.listBooks();
        
        assertThat(actualList, equalTo(expectedList));
    }
    
    
    
    @Test
    public void findingBooksById_ShouldReturn_MockBook1() throws Exception {
        IBook expected = mockBook1_;
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, mockBook1_);
        bookMap_.put(BOOK_ID_TWO, mockBook2_);
        bookMap_.put(BOOK_ID_THREE, mockBook3_);
        
        IBook actual = sut_.getBookByID(BOOK_ID_ONE);
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test
    public void findingBooksByAuthor_ShouldThrowExceptionWhen_AuthorIsNull(){
       thrown.expect(IllegalArgumentException.class);
        
       sut_.findBooksByAuthor(null);
       
       fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void findingBooksByAuthor_ShouldReturnEmptyListWhen_BookNotFound() throws Exception {
        List<IBook> expectedEmptyList = new LinkedList<IBook>();
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, mockBook1_);
        bookMap_.put(BOOK_ID_TWO, mockBook2_);
        bookMap_.put(BOOK_ID_THREE, mockBook3_);
        
        List<IBook> actualList = sut_.findBooksByAuthor("Richard Richardson");
        
        assertThat(actualList, equalTo(expectedEmptyList));
    }
    
    
    
    @Test
    public void findingBooksByAuthor_ShouldReturn_MockBook3() throws Exception {
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, mockBook1_);
        bookMap_.put(BOOK_ID_TWO, mockBook2_);
        bookMap_.put(BOOK_ID_THREE, mockBook3_);
        
        List<IBook> actualList = sut_.findBooksByAuthor("Jim Jimson");
        
        assertThat(actualList, hasItem(mockBook3_));
    }
    
    
    
    @Test
    public void findingBooksByTitle_ShouldThrowExceptionWhen_TitleIsNull(){
       thrown.expect(IllegalArgumentException.class);
        
       sut_.findBooksByTitle(null);
       
       fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void findingBooksByTitle_ShouldReturnEmptyListWhen_BookNotFound() throws Exception {
        List<IBook> expectedEmptyList = new LinkedList<IBook>();
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, mockBook1_);
        bookMap_.put(BOOK_ID_TWO, mockBook2_);
        bookMap_.put(BOOK_ID_THREE, mockBook3_);
        
        List<IBook> actualList = sut_.findBooksByTitle("I don't exist");
        
        assertThat(actualList, equalTo(expectedEmptyList));
    }
    
    
    
    @Test
    public void findingBooksByTitle_ShouldReturn_MockBook1() throws Exception {
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, mockBook1_);
        bookMap_.put(BOOK_ID_TWO, mockBook2_);
        bookMap_.put(BOOK_ID_THREE, mockBook3_);
        
        List<IBook> actualList = sut_.findBooksByTitle("Tales of Jim Jimson");
        
        assertThat(actualList, hasItem(mockBook1_));
    }
    
    
    
    @Test
    public void findingBooksByAuthorAndTitle_ShouldThrowExceptionWhen_AuthorIsNull(){
       thrown.expect(IllegalArgumentException.class);
        
       sut_.findBooksByAuthorTitle(null, "Tales of Jim Jimson");
       
       fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void findingBooksByAuthorAndTitle_ShouldThrowExceptionWhen_TitleIsNull(){
       thrown.expect(IllegalArgumentException.class);
        
       sut_.findBooksByAuthorTitle("Bob Bobson", null);
       
       fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void findingBooksByAuthorAndTitle_ShouldReturnEmptyListWhen_BookNotFound() throws Exception {
        List<IBook> expectedEmptyList = new LinkedList<IBook>();
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, mockBook1_);
        bookMap_.put(BOOK_ID_TWO, mockBook2_);
        bookMap_.put(BOOK_ID_THREE, mockBook3_);
        
        List<IBook> actualList = sut_.findBooksByAuthorTitle("Richard Richardson", "Tales of Jim Jimson");
        
        assertThat(actualList, equalTo(expectedEmptyList));
    }
    
    
    
    @Test
    public void findingBooksByAuthorAndTitle_ShouldReturn_MockBook1() throws Exception {
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, mockBook1_);
        bookMap_.put(BOOK_ID_TWO, mockBook2_);
        bookMap_.put(BOOK_ID_THREE, mockBook3_);
        
        List<IBook> actualList = sut_.findBooksByAuthorTitle("Bob Bobson", "Tales of Jim Jimson");
        
        assertThat(actualList, hasItem(mockBook1_));
    }
}
