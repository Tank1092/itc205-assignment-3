package library.daos;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import library.interfaces.entities.IBook;
import library.interfaces.daos.IBookHelper;

// BookHelper integration test with Book
public class BookHelperBookIntegrationTest {
    
    private IBookHelper sut_;
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    
    
    @Before
    public void setUp() throws Exception {
        sut_ = new BookHelper();
    }

    
    
    @After
    public void tearDown() throws Exception {
        sut_ = null;
    }

    
    
    @Test
    public void attemptingToCreateBook_BookShouldNotBeNull() throws Exception {
        final int BOOK_ID = 1;
        IBook book = sut_.makeBook("Bob Bobson", "Tales of Jim Jimson", "123456789", BOOK_ID);
        
        assertNotNull(book);
    }
    
    
    
    @Test
    public void attemptingToCreateBook_ShouldThrowExceptionWhen_ParameterInvalid() {
        thrown.expect(IllegalArgumentException.class);
        final int INVALID_ID = -1;
        
        IBook book = sut_.makeBook("Bob Bobson", "Tales of Jim Jimson", "123456789", INVALID_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void attemptingToCreateBook_BookContainsSuppliedAuthor() throws Exception {
        String expectedAuthor = "Bob Bobson";
        final int BOOK_ID = 1;
        
        IBook book = sut_.makeBook("Bob Bobson", "Tales of Jim Jimson", "123456789", BOOK_ID);
        String actualAuthor = book.getAuthor();
        
        assertThat(actualAuthor, equalTo(expectedAuthor));
    }
    
    
    
    @Test
    public void attemptingToCreateBook_BookContainsSuppliedTitle() throws Exception {
        String expectedTitle = "Tales of Jim Jimson";
        final int BOOK_ID = 1;
        
        IBook book = sut_.makeBook("Bob Bobson", "Tales of Jim Jimson", "123456789", BOOK_ID);
        String actualTitle = book.getTitle();
        
        assertThat(actualTitle, equalTo(expectedTitle));
    }
    
    
    
    @Test
    public void attemptingToCreateBook_BookContainsSuppliedCallNumber() throws Exception {
        String expectedCallNumber = "123456789";
        final int BOOK_ID = 1;
        
        IBook book = sut_.makeBook("Bob Bobson", "Tales of Jim Jimson", "123456789", BOOK_ID);
        String actualCallNumber = book.getCallNumber();
       
        assertThat(actualCallNumber, equalTo(expectedCallNumber));
    }
    
    
    
    @Test
    public void attemptingToCreateBook_BookContainsSuppliedId() throws Exception {
        int expectedId = 1;
        final int BOOK_ID = 1;
        
        IBook book = sut_.makeBook("Bob Bobson", "Tales of Jim Jimson", "123456789", BOOK_ID);
        int actualId = book.getID();
        
        assertThat(actualId, equalTo(expectedId));
    }
}
