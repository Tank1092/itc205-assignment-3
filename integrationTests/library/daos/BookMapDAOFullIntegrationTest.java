package library.daos;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import library.daos.BookMapDAO;
import library.entities.Book;
import library.interfaces.daos.IBookHelper;
import library.interfaces.entities.IBook;

import java.util.List;
import java.util.LinkedList;
import java.util.HashMap;

// BookMapDAO full integration test with BookHelper and Book
@RunWith(MockitoJUnitRunner.class)
public class BookMapDAOFullIntegrationTest {

	
    private IBook book1, book2, book3;
    
    @Spy
    private IBookHelper helper_ = new BookHelper();
    
    @Spy
    private HashMap<Integer, IBook> bookMap_;
    
    @InjectMocks
    protected BookMapDAO sut_ = new BookMapDAO(helper_);
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    
    
    @Before
    public void setUp() throws Exception {     
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        
        book1 = new Book("Bob Bobson", "Tales of Jim Jimson", "12334567890", BOOK_ID_ONE);
        book2 = new Book("Bob Bobson", "The Secret Life of Jim Jimson", "5932871", BOOK_ID_TWO);
        book3 = new Book("Jim Jimson", "The Shadow: My encounter with a stalker", "00000000", BOOK_ID_THREE);
    }
    
    
    
    @After
    public void tearDown() throws Exception {
        helper_ = null;
        bookMap_ = null;
        sut_ = null;
    }
    
    
    
    @Test
    public void instantiatingDAO_ShouldThrowExceptionWhen_HelperIsNull() {
        thrown.expect(IllegalArgumentException.class);
        
        new BookMapDAO(null);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test 
    public void addingBook_ShouldAddBookToMap() throws Exception {
        String expectedAuthor = "Bob Bobson";
        String expectedTitle = "Tales of Jim Jimson";
        String expectedCallNumber = "12334567890";
        int expectedId = 1;
        Object[] expectedItems = {expectedAuthor, expectedTitle, expectedCallNumber, expectedId};
        
        sut_.addBook("Bob Bobson", "Tales of Jim Jimson", "12334567890");
        IBook book = bookMap_.get(1);
        Object[] actualItems = {book.getAuthor(), book.getTitle(), book.getCallNumber(), book.getID()};
        
        assertThat(actualItems, equalTo(expectedItems));
    }
    
    
    
    @Test 
    public void addingBook_ShouldNotAddBookToMapWhen_ArgumentInvalid() throws Exception {
        final int BOOK_ID = 1;
        
        sut_.addBook(" ", "Tales of Jim Jimson", "12334567890");
        IBook actual = bookMap_.get(BOOK_ID);
        
        assertNull(actual);
    }
    
    
    
    @Test 
    public void listOfBooks_ShouldContain_NoBooks() throws Exception {
        List<IBook> expectedEmptyList = new LinkedList<IBook>();
        
        List<IBook> actualList = sut_.listBooks();
        
        assertThat(actualList, equalTo(expectedEmptyList));
    }
    
    
    
    @Test 
    public void listOfBooks_ShouldContain_Book1() throws Exception {
        List<IBook> expectedList = new LinkedList<IBook>();
        final int BOOK_ID = 1;
        expectedList.add(book1);
        bookMap_.put(BOOK_ID, book1);
        
        List<IBook> actualList = sut_.listBooks();
        
        assertThat(actualList, equalTo(expectedList));
    }
    
    
    
    @Test
    public void findingBooksById_ShouldReturn_Book1() throws Exception {
        IBook expected = book1;
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, book1);
        bookMap_.put(BOOK_ID_TWO, book2);
        bookMap_.put(BOOK_ID_THREE, book3);
        
        IBook actual = sut_.getBookByID(1);
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test
    public void findingBooksByAuthor_ShouldThrowExceptionWhen_AuthorIsNull(){
       thrown.expect(IllegalArgumentException.class);
        
       sut_.findBooksByAuthor(null);
       
       fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void findingBooksByAuthor_ShouldReturnEmptyListWhen_BookNotFound() throws Exception {
        List<IBook> expectedEmptyList = new LinkedList<IBook>();
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, book1);
        bookMap_.put(BOOK_ID_TWO, book2);
        bookMap_.put(BOOK_ID_THREE, book3);
        
        List<IBook> actualList = sut_.findBooksByAuthor("Richard Richardson");
        
        assertThat(actualList, equalTo(expectedEmptyList));
    }
    
    
    
    @Test
    public void findingBooksByAuthor_ShouldReturn_Book3() throws Exception {
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, book1);
        bookMap_.put(BOOK_ID_TWO, book2);
        bookMap_.put(BOOK_ID_THREE, book3);
        
        List<IBook> actualList = sut_.findBooksByAuthor("Jim Jimson");
        
        assertThat(actualList, hasItem(book3));
    }
    
    
    
    @Test
    public void findingBooksByTitle_ShouldThrowExceptionWhen_TitleIsNull(){
       thrown.expect(IllegalArgumentException.class);
        
       sut_.findBooksByTitle(null);
       
       fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void findingBooksByTitle_ShouldReturnEmptyListWhen_BookNotFound() throws Exception {
        List<IBook> expectedEmptyList = new LinkedList<IBook>();
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, book1);
        bookMap_.put(BOOK_ID_TWO, book2);
        bookMap_.put(BOOK_ID_THREE, book3);
        
        List<IBook> actualList = sut_.findBooksByTitle("I don't exist");
        
        assertThat(actualList, equalTo(expectedEmptyList));
    }
    
    
    
    @Test
    public void findingBooksByTitle_ShouldReturn_Book1() throws Exception {
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, book1);
        bookMap_.put(BOOK_ID_TWO, book2);
        bookMap_.put(BOOK_ID_THREE, book3);
        
        List<IBook> actualList = sut_.findBooksByTitle("Tales of Jim Jimson");
        
        assertThat(actualList, hasItem(book1));
    }
    
    
    
    @Test
    public void findingBooksByAuthorAndTitle_ShouldThrowExceptionWhen_AuthorIsNull(){
       thrown.expect(IllegalArgumentException.class);
        
       sut_.findBooksByAuthorTitle(null, "Tales of Jim Jimson");
       
       fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void findingBooksByAuthorAndTitle_ShouldThrowExceptionWhen_TitleIsNull(){
       thrown.expect(IllegalArgumentException.class);
        
       sut_.findBooksByAuthorTitle("Bob Bobson", null);
       
       fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void findingBooksByAuthorAndTitle_ShouldReturnEmptyListWhen_BookNotFound() throws Exception {
        List<IBook> expectedEmptyList = new LinkedList<IBook>();
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, book1);
        bookMap_.put(BOOK_ID_TWO, book2);
        bookMap_.put(BOOK_ID_THREE, book3);
        
        List<IBook> actualList = sut_.findBooksByAuthorTitle("Richard Richardson", "Tales of Jim Jimson");
        
        assertThat(actualList, equalTo(expectedEmptyList));
    }
    
    
    
    @Test
    public void findingBooksByAuthorAndTitle_ShouldReturn_Book1() throws Exception {
        final int BOOK_ID_ONE = 1;
        final int BOOK_ID_TWO = 2;
        final int BOOK_ID_THREE = 3;
        bookMap_.put(BOOK_ID_ONE, book1);
        bookMap_.put(BOOK_ID_TWO, book2);
        bookMap_.put(BOOK_ID_THREE, book3);
        
        List<IBook> actualList = sut_.findBooksByAuthorTitle("Bob Bobson", "Tales of Jim Jimson");
        
        assertThat(actualList, hasItem(book1));
    }
}

