package library;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import library.daos.MemberMapDAO;
import library.daos.MemberHelper;

import library.interfaces.EBorrowState;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.IBookHelper;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.ILoanHelper;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.daos.IMemberHelper;
import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.interfaces.hardware.ICardReader;
import library.interfaces.hardware.IDisplay;
import library.interfaces.hardware.IPrinter;
import library.interfaces.hardware.IScanner;

import java.util.List;
import java.util.ArrayList;

// BorrowUC_CTL integration test with MebmerDAO (and its dependencies)
@RunWith(MockitoJUnitRunner.class)
public class BorrowUC_CTLMemberDAOIntegrationTest {
    
    @Mock
    private ICardReader reader_ = mock(ICardReader.class);
    
    @Mock
    private IScanner scanner_ = mock(IScanner.class);
    
    @Mock
    private IPrinter printer_ = mock(IPrinter.class);
    
    @Mock
    private IDisplay display_ = mock(IDisplay.class);
    
    @Mock
    private IBookHelper bookHelper_ = mock(IBookHelper.class);
    
    @Mock
    private ILoanHelper loanHelper_ = mock(ILoanHelper.class);
    
    @Spy
    private IMemberHelper memberHelper_ = new MemberHelper();
    
    @Mock
    private IBookDAO bookDAO_ = mock(IBookDAO.class);
    
    @Mock
    private ILoanDAO loanDAO_ = mock(ILoanDAO.class);
    
    @Spy
    private IMemberDAO memberDAO_ = new MemberMapDAO(memberHelper_);
    
    @Mock 
    private IBook mockBook1_, mockBook2_;
    
    @Mock 
    private ILoan mockLoan1_, mockLoan2_;
     
    private IMember member1_, member2_, member3_, member4_, member5_;
    
                                // The member added to the map here is the original, NOT the spy
                                // Used for tests which require the CTL to have a borrower as not null
    @Spy                        // Also used to inject loans into
    private IMember borrower_ = spy(memberDAO_.addMember("Jim", "Jimson", "1111-222-333", "Jim@Jimson.com")); 
    
    @Spy
    private List<IBook> bookList_ = new ArrayList<IBook>();
    
    @Spy
    private List<ILoan> loanList_ = new ArrayList<ILoan>();
    
    @InjectMocks
    private BorrowUC_CTL sut_ = new BorrowUC_CTL(reader_, scanner_, printer_, display_,
                                                 bookDAO_, loanDAO_, memberDAO_);
    
    @Mock 
    private BorrowUC_UI ui_ = mock(BorrowUC_UI.class); 
    
    private List<ILoan> mockMemberLoans_;
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    
    
    @Before
    public void setUp() throws Exception {
        mockMemberLoans_ = new ArrayList<ILoan>();
        
        // Setup the DAO's
        when(bookDAO_.getBookByID(1)).thenReturn(mockBook1_);
        when(bookDAO_.getBookByID(2)).thenReturn(mockBook2_);
        
        when(loanDAO_.getLoanByID(1)).thenReturn(mockLoan1_);
        when(loanDAO_.getLoanByID(2)).thenReturn(mockLoan2_);
        
        // If a loan is created at any point, just use mockLoan1_ to represent it
        when(loanDAO_.createLoan(anyObject(), anyObject())).thenReturn(mockLoan1_);
        
        // Setup the mock loans
        when(mockLoan1_.toString()).thenReturn("Mock Loan 1 Details");
        when(mockLoan2_.toString()).thenReturn("Mock Loan 2 Details");
        when(mockLoan2_.isOverDue()).thenReturn(true);  // Make mockLoan2_ overdue
        
        // Setup the mock books
        when(mockBook1_.getState()).thenReturn(EBookState.AVAILABLE); // Make mockBook1_ available
        
        // Setup the members
        member1_ = borrower_;                                                                               // No loans   
        member2_ = memberDAO_.addMember("Bob", "Bobson", "4444-555-666", "Bob@Bobson.com");                 // Overdue loan
        member3_ = memberDAO_.addMember("Richard", "Richardson", "7777-888-999", "Richard@Richardson.com"); // Has fines but under limit
        member4_ = memberDAO_.addMember("Kelly", "Richardson", "1234-567-890", "Kelly@Richardson.com");     // At maximum fine limit
        member5_ = memberDAO_.addMember("Pamela", "Johnson", "0000-000-000", "Pam@Bigpond.com.au");         // At loan limit
        
        
        // Force the DAO to return the spy, instead of the original
        when(memberDAO_.getMemberByID(1)).thenReturn(member1_); 
        
        // Return a fake list of loans for this member
        when(member1_.getLoans()).thenReturn(mockMemberLoans_);
        
        // Give member 2 an overdue loan
        member2_.addLoan(mockLoan2_);
        
        // Give member 3 a small fine
        memberDAO_.getMemberByID(3).addFine(3.0f);
        
        // Give member 4 the max fine
        memberDAO_.getMemberByID(4).addFine(IMember.FINE_LIMIT);
        
        // Give member 5 the maximum amount of loans
        for (int i = 0; i < IMember.LOAN_LIMIT; i++){
            member5_.addLoan(mock(ILoan.class));
        }
    }
 
    
    
    @After
    public void tearDown() throws Exception {
        // Everything is made null here to make it clear that nothing should 'carry over' between tests
        reader_ = null;
        scanner_ = null;
        printer_ = null;
        display_ = null;
        
        bookHelper_ = null;
        loanHelper_ = null;
        memberHelper_ = null;
        
        bookDAO_ = null;
        loanDAO_ = null;
        memberDAO_ = null;
        
        mockBook1_ = null;  
        mockBook2_ = null;
        
        mockLoan1_ = null;
        mockLoan2_ = null;
        
        member1_ = null;
        member2_ = null;
        member3_ = null;
        member4_ = null;
        member5_ = null;
        
        borrower_ = null;
        bookList_ = null;
        loanList_ = null;
        
        sut_ = null;
        ui_ = null;
        
        mockMemberLoans_ = null;
    }
    
    /// ******* INITIALISATION TESTS ******* ///
    
    @Test
    public void initialisation_ShouldDisplay_BorrowingUI() throws Exception {
        sut_.initialise();
        
        verify(display_).setDisplay(ui_, "Borrow UI");
    }
    
    
    
    @Test
    public void initialisation_ShouldEnable_CardReader() throws Exception {
        sut_.initialise();
        
        verify(reader_).setEnabled(true);
    }
    
    
    
    @Test
    public void initialisation_ShouldDisable_Scanner() throws Exception {
        sut_.initialise();
        
        verify(scanner_).setEnabled(false);
    }
    
    
    
    @Test
    public void initialisation_ShouldSetCTLStateTo_INITIALIZED() throws Exception {
        EBorrowState expected = EBorrowState.INITIALIZED;
        
        sut_.initialise();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test
    public void initialisation_ShouldSetUIStateTo_INITIALIZED() throws Exception {
        EBorrowState expected = EBorrowState.INITIALIZED;
        
        sut_.initialise();
        
        verify(ui_).setState(expected);
    }
    
    
    /// ******* CARD SWIPING TESTS ******* ///
    
    
    @Test
    public void swipingCard_ShouldThrowExceptionWhen_StateNot_INITIALIZED() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        final int ARBITRARY_ID = 1;
        
        sut_.cardSwiped(ARBITRARY_ID);
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test
    public void swipingCard_ShouldDisplay_ErrorMessageWhen_MemberDoesNotExist() throws Exception {
        final int ARBITRARY_ID = 999;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(ARBITRARY_ID);
        
        verify(ui_).displayErrorMessage(contains("not found"));
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisable_CardReader() throws Exception {
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(reader_).setEnabled(false);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisable_Scanner() throws Exception {
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(scanner_).setEnabled(false);
    }



    @Test 
    public void swipingCard_ShouldSetCTLStateTo_SCANNING_BOOKS() throws Exception {
        final int MEMBER_ID = 1;
        EBorrowState expected = EBorrowState.SCANNING_BOOKS;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void swipingCard_ShouldSetCTLStateTo_BORROWING_RESTRICTED_WhenMemberRestricted() throws Exception {
        final int MEMBER_ID = 2;
        EBorrowState expected = EBorrowState.BORROWING_RESTRICTED;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void swipingCard_ShouldSetUIStateTo_SCANNING_BOOKS() throws Exception {
        final int MEMBER_ID = 1;
        EBorrowState expected = EBorrowState.SCANNING_BOOKS;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).setState(expected);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldSetUIStateTo_BORROWING_RESTRICTED_WhenMemberRestricted() throws Exception {
        final int MEMBER_ID = 2;
        EBorrowState expected = EBorrowState.BORROWING_RESTRICTED;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).setState(expected);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldSetScanCountTo_NumberOfExistingLoans() throws Exception {
        mockMemberLoans_.add(mockLoan1_);
        mockMemberLoans_.add(mockLoan2_);
        final int MEMBER_ID = 1;
        int expected = mockMemberLoans_.size();
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        int actual = sut_.getScanCount();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_Member1Details() throws Exception {
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayMemberDetails(MEMBER_ID, "Jim Jimson", "1111-222-333");
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_ExistingLoans() throws Exception {
        mockMemberLoans_.add(mockLoan1_);
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayExistingLoan("Mock Loan 1 Details");
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_NoExistingLoanDetailsWhen_ExistingLoanListEmpty() throws Exception {
        final int MEMBER_ID = 1;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayExistingLoan("");
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_OutstandingFineMessageWhen_MemberHasFines() throws Exception {
        final int MEMBER_ID = 3;
        float expectedAmount = member3_.getFineAmount();
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayOutstandingFineMessage(expectedAmount);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_OverdueMessageWhen_MemberRestrictedAndHasOverdueBooks() throws Exception {
        final int MEMBER_ID = 2;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayOverDueMessage();
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_AtLoanLimitMessageWhen_MemberRestrictedAndAtLoanLimit() throws Exception {
        final int MEMBER_ID = 5;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayAtLoanLimitMessage();
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_OverFineLimitMessageWhen_MemberRestrictedAndOverFineLimit() throws Exception {
        final int MEMBER_ID = 4;
        float expectedAmount = member4_.getFineAmount();
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayOverFineLimitMessage(expectedAmount);
    }
    
    
    
    @Test 
    public void swipingCard_ShouldDisplay_BorrowingRestrictedMessageWhen_MemberRestricted() throws Exception {
        final int MEMBER_ID = 5;
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.cardSwiped(MEMBER_ID);
        
        verify(ui_).displayErrorMessage(contains("Borrowing restricted"));
    }
    
    
    /// ******* BOOK SCANNING TESTS ******* ///
    
    
    @Test 
    public void scanningBook_ShouldThrowExceptionWhen_StateNot_SCANNING_BOOKS() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.INITIALIZED);
        final int ARBITRARY_ID = 1;
        
        sut_.bookScanned(ARBITRARY_ID);
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test 
    public void scanningBook_ShouldIncrement_ScanCountByOne() throws Exception {
        int expected = 4;
        sut_.setScanCount(expected - 1);
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        int actual = sut_.getScanCount();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisplay_ConfirmLoanUI_AfterLoanLimitReached() throws Exception {
        sut_.setScanCount(4);
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        EBorrowState expected = EBorrowState.CONFIRMING_LOANS;
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).setState(expected);
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisable_CardReader() throws Exception {
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(reader_).setEnabled(false);
    }
    
    
    
    @Test 
    public void scanningBook_ShouldEnable_Scanner() throws Exception {
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(scanner_).setEnabled(true);
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisplay_ErrorMessageWhen_BookNotFound() throws Exception {
        final int BOOK_ID = 5;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).displayErrorMessage(contains("not found"));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisplay_ErrorMessageWhen_BookNotAvailable() throws Exception {
        final int BOOK_ID = 2;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).displayErrorMessage(contains("not available"));
    }

    
    
    @Test 
    public void scanningBook_ShouldDisplay_ErrorMessageWhen_BookAlreadyScanned() throws Exception {
        final int BOOK_ID = 1;
        bookList_.add(mockBook1_);
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).displayErrorMessage(contains("already scanned"));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldAddBookTo_ScannedBookList() throws Exception {
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        assertThat(bookList_, hasItem(mockBook1_));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldCreate_NewPendingLoan() throws Exception {
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        assertThat(loanList_, hasItem(mockLoan1_));
    }
    
    
    
    @Test 
    public void scanningBook_ShouldDisplay_PendingLoanList() throws Exception {
        String expectedLoan1 = mockLoan1_.toString();
        final int BOOK_ID = 1;
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.bookScanned(BOOK_ID);
        
        verify(ui_).displayPendingLoan(contains(expectedLoan1));
    }
    
    
    /// ******* SCAN COMPLETION TESTS ******* ///
    
    
    @Test 
    public void completingScan_ShouldThrowExceptionWhen_StateNot_SCANNING_BOOKS() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.scansCompleted();
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test 
    public void completingScan_ShouldDisplay_ConfirmLoanUI() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        EBorrowState expected = EBorrowState.CONFIRMING_LOANS;
        
        sut_.scansCompleted();
        
        verify(ui_).setState(expected);
    }
    
    
    
    @Test 
    public void completingScan_ShouldDisplay_PendingLoanList() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        loanList_.add(mockLoan1_);
        loanList_.add(mockLoan2_);
        String expectedLoan1 = mockLoan1_.toString();
        String expectedLoan2 = mockLoan2_.toString();
        
        sut_.scansCompleted();
        
        verify(ui_).displayConfirmingLoan(contains(expectedLoan1));
        verify(ui_).displayConfirmingLoan(contains(expectedLoan2));
    }
    
    
    
    @Test 
    public void completingScan_ShouldDisable_CardReader() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.scansCompleted();
        
        // atMost quantifier needed because of earlier setState call
        verify(reader_, atMost(2)).setEnabled(false); 
    }
    
    
    
    @Test 
    public void completingScan_ShouldDisable_Scanner() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        
        sut_.scansCompleted();
        
        verify(scanner_).setEnabled(false);
    }
    
    
    
    @Test 
    public void completingScan_ShouldSetCTLStateTo_CONFIRMING_LOANS() throws Exception {
        sut_.setState(EBorrowState.SCANNING_BOOKS);
        EBorrowState expected = EBorrowState.CONFIRMING_LOANS;
        
        sut_.scansCompleted();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    /// ******* LOAN REJECTION TESTS ******* ///
    
    
    @Test 
    public void rejectingLoans_ShouldThrowExceptionWhen_StateNot_CONFIRMING_LOANS() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.loansRejected();
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldDisplay_BorrowingUI() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        EBorrowState expected = EBorrowState.SCANNING_BOOKS;
        
        sut_.loansRejected();
        
        verify(ui_).setState(expected);
    }

    
    
    @Test 
    public void rejectingLoans_ShouldClear_PendingLoanList() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        final int ZERO = 0;
        loanList_.add(mockLoan1_);
        
        sut_.loansRejected();
        
        assertThat(loanList_.size(), equalTo(ZERO));
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldClear_ScannedBookList() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        final int ZERO = 0;
        bookList_.add(mockBook1_);
        
        sut_.loansRejected();
        
        assertThat(bookList_.size(), equalTo(ZERO));
    }
        
    
    
    @Test 
    public void rejectingLoans_ShouldSetScanCountTo_NumberOfExistingLoans() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        mockMemberLoans_.add(mockLoan1_);
        mockMemberLoans_.add(mockLoan2_);
        int expected = mockMemberLoans_.size();
        sut_.setScanCount(expected + 1);
        
        sut_.loansRejected();
        int actual = sut_.getScanCount();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldDisable_CardReader() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansRejected();
        
        // atMost quantifier needed because of earlier setState call
        verify(reader_, atMost(2)).setEnabled(false);
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldEnable_Scanner() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansRejected();
        
        // atMost quantifier needed because of earlier setState call
        verify(scanner_, atMost(2)).setEnabled(true);
    }
    
    
    
    @Test 
    public void rejectingLoans_ShouldSetCTLStateTo_SCANNING_BOOKS() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        EBorrowState expected = EBorrowState.SCANNING_BOOKS;
        
        sut_.loansRejected();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    /// ******* LOAN CONFIRMATION TESTS ******* ///
    
    
    @Test 
    public void confirmingLoans_ShouldThrowExceptionWhen_StateNot_CONFIRMING_LOANS() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBorrowState.INITIALIZED);
        
        sut_.loansConfirmed();
        
        fail("Exception should have been thrown");
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldCommit_PendingLoans() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        loanList_.add(mockLoan1_);
        loanList_.add(mockLoan2_);
        
        sut_.loansConfirmed();
        
        verify(loanDAO_).commitLoan(mockLoan1_);
        verify(loanDAO_).commitLoan(mockLoan2_);
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldDisplay_MainMenu() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansConfirmed();
        
        verify(display_).setDisplay(anyObject(), eq("Main Menu"));
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldDisplay_ListOfCommittedLoans() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        loanList_.add(mockLoan1_);
        loanList_.add(mockLoan2_);
        String expectedLoan1 = mockLoan1_.toString();
        String expectedLoan2 = mockLoan2_.toString();
        
        sut_.loansConfirmed();
        
        verify(printer_).print(contains(expectedLoan1));
        verify(printer_).print(contains(expectedLoan2));
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldDisable_CardReader() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansConfirmed();
        
        // atMost quantifier needed because of earlier setState call
        verify(reader_, atMost(2)).setEnabled(false);
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldDisable_Scanner() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        
        sut_.loansConfirmed();
        
        // atMost quantifier needed because of earlier setState call
        verify(scanner_, atMost(2)).setEnabled(false);
    }
    
    
    
    @Test 
    public void confirmingLoans_ShouldSetCTLStateTo_COMPLETED() throws Exception {
        sut_.setState(EBorrowState.CONFIRMING_LOANS);
        EBorrowState expected = EBorrowState.COMPLETED;
        
        sut_.loansConfirmed();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
    
    
    /// ******* CANCEL OPERATION TESTS ******* ///
    
    
    @Test 
    public void cancelling_ShouldDisplay_MainMenu() throws Exception {
        sut_.cancelled();
        
        verify(display_).setDisplay(anyObject(), eq("Main Menu"));
    }
    
    
    
    @Test 
    public void cancelling_ShouldDisable_Reader() throws Exception {
        sut_.cancelled();
        
        verify(reader_).setEnabled(false);
    }
    
    
    
    @Test 
    public void cancelling_ShouldDisable_Scanner() throws Exception {
        sut_.cancelled();
        
        verify(scanner_).setEnabled(false);
    }
    
    
    
    @Test 
    public void cancelling_ShouldSetCTLStateTo_CANCELLED() throws Exception {
        sut_.setState(EBorrowState.COMPLETED);
        EBorrowState expected = EBorrowState.CANCELLED;
        
        sut_.cancelled();
        EBorrowState actual = sut_.getState();
        
        assertThat(actual, equalTo(expected));
    }
}
