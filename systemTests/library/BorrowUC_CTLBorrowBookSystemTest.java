package library;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import library.daos.BookMapDAO;
import library.daos.BookHelper;
import library.daos.LoanMapDAO;
import library.daos.LoanHelper;
import library.daos.MemberMapDAO;
import library.daos.MemberHelper;

import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.IBookHelper;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.ILoanHelper;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.daos.IMemberHelper;
import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.interfaces.hardware.ICardReader;
import library.interfaces.hardware.IDisplay;
import library.interfaces.hardware.IPrinter;
import library.interfaces.hardware.IScanner;
import library.panels.MainPanel;
import library.hardware.CardReader;
import library.hardware.Scanner;
import library.hardware.Printer;
import library.hardware.Display;

import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


@RunWith(MockitoJUnitRunner.class)
public class BorrowUC_CTLBorrowBookSystemTest {

    @Spy
    private ICardReader reader_ = new CardReader();
    
    @Spy
    private IScanner scanner_ = new Scanner();
    
    @Spy
    private IPrinter printer_ = new Printer();
    
    @Spy
    private IDisplay display_ = new Display();
    
    private IBookHelper bookHelper_ = new BookHelper();
    
    private ILoanHelper loanHelper_ = new LoanHelper();
    
    private IMemberHelper memberHelper_ = new MemberHelper();
    
    @Spy
    private IBookDAO bookDAO_ = new BookMapDAO(bookHelper_);
    
    @Spy
    private ILoanDAO loanDAO_ = new LoanMapDAO(loanHelper_); 
    
    @Spy
    private IMemberDAO memberDAO_ = new MemberMapDAO(memberHelper_);
    
    private IBook book1_, book2_, book3_, book4_, book5_, book6_, book7_, book8_;
    
    private ILoan loan1_, loan2_, loan3_, loan4_, loan5_, loan6_;
 
    private IMember member1_, member2_, member3_, member4_, member5_;
    
                                // The member added to the map here is the original, NOT the spy
                                // Used for tests which require the CTL to have a borrower as not null
    @Spy                        // Also used to inject loans into
    private IMember borrower_ = spy(memberDAO_.addMember("Jim", "Jimson", "1111-222-333", "Jim@Jimson.com")); 
    
    @Spy
    private List<IBook> bookList_ = new ArrayList<IBook>();
    
    @Spy
    private List<ILoan> loanList_ = new ArrayList<ILoan>();
    
    @InjectMocks
    private BorrowUC_CTL sut_ = new BorrowUC_CTL(reader_, scanner_, printer_, display_,
                                                 bookDAO_, loanDAO_, memberDAO_);
    
    @Spy 
    private BorrowUC_UI ui_ = new BorrowUC_UI(sut_); 
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    
    
    @Before
    public void setUp() throws Exception {
        // Initialise the GUI as normal
        Main main = new Main();
        display_.setDisplay(new MainPanel(main), "Main Menu");
        
        // Setup the books
        book1_ = bookDAO_.addBook("Bob Bobson", "Tales of Jim Jimson", "12334567890"); // Not on loan
        book2_ = bookDAO_.addBook("Bob Bobson", "The Secret Life of Jim Jimson", "5932871");
        book3_ = bookDAO_.addBook("Jim Jimson", "The Shadow: My encounter with a stalker", "00000000");
        book4_ = bookDAO_.addBook("Anon", "The fascinating story of pizza and cheese part 1", "00000001");
        book5_ = bookDAO_.addBook("Anon", "The fascinating story of pizza and cheese part 2", "00000002");
        book6_ = bookDAO_.addBook("Richard Richardson", "I think I lost my toothbrush", "16765472");
        book7_ = bookDAO_.addBook("Pamela Johnson", "A walk in the dark", "1564343");
        book8_ = bookDAO_.addBook("Kelly Richardson", "Some other nonsense title", "12315"); // Not on loan 
        
        // Setup the members
        member1_ = borrower_;                                                                               // No loans   
        member2_ = memberDAO_.addMember("Bob", "Bobson", "4444-555-666", "Bob@Bobson.com");                 // Overdue loan
        member3_ = memberDAO_.addMember("Richard", "Richardson", "7777-888-999", "Richard@Richardson.com"); // Has fines but under limit
        member4_ = memberDAO_.addMember("Kelly", "Richardson", "1234-567-890", "Kelly@Richardson.com");     // At maximum fine limit, no loans
        member5_ = memberDAO_.addMember("Pamela", "Johnson", "0000-000-000", "Pam@Bigpond.com.au");         // One book shy of loan limit
        
        // Force the DAO to return the spy, instead of the original
        when(memberDAO_.getMemberByID(1)).thenReturn(member1_); 
        
        loan1_ = loanDAO_.createLoan(member2_, book3_);    // Overdue
        loan2_ = loanDAO_.createLoan(member3_, book5_);    
        
        // Code from Main.java - Thanks Jim!
        // Make member2's loan be overdue
        Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();
        
        loanDAO_.commitLoan(loan1_);
        cal.setTime(now);
        cal.add(Calendar.DATE, ILoan.LOAN_PERIOD + 1);
        Date checkDate = cal.getTime();     
        loanDAO_.updateOverDueStatus(checkDate);
        
        // Give member 3 a small fine
        memberDAO_.getMemberByID(3).addFine(3.0f);
        
        // Give member 4 the max fine
        memberDAO_.getMemberByID(4).addFine(10.0f); 
        
        // Put member 5 almost at the loan limit
        loan3_ = loanDAO_.createLoan(member5_, book2_);
        loan4_ = loanDAO_.createLoan(member5_, book4_);
        loan5_ = loanDAO_.createLoan(member5_, book6_);
        loan6_ = loanDAO_.createLoan(member5_, book7_);
        
        // Record/confirm all the loans
        loanDAO_.commitLoan(loan2_);
        loanDAO_.commitLoan(loan3_);
        loanDAO_.commitLoan(loan4_);
        loanDAO_.commitLoan(loan5_);
        loanDAO_.commitLoan(loan6_);
    }

    
    
    @After
    public void tearDown() throws Exception {
        // Everything is made null here to make it clear that nothing should 'carry over' between tests
        reader_ = null;
        scanner_ = null;
        printer_ = null;
        display_ = null;
        
        bookHelper_ = null;
        loanHelper_ = null;
        memberHelper_ = null;
        
        bookDAO_ = null;
        loanDAO_ = null;
        memberDAO_ = null;
        
        book1_ = null;  
        book2_ = null;
        book3_ = null;
        book4_ = null;  
        book5_ = null;
        book6_ = null;
        book7_ = null;
        book8_ = null;
        
        loan1_ = null;
        loan2_ = null;
        loan3_ = null;
        loan4_ = null;
        loan5_ = null;
        loan6_ = null;
        
        member1_ = null;
        member2_ = null;
        member3_ = null;
        member4_ = null;
        member5_ = null;
        
        borrower_ = null;
        bookList_ = null;
        loanList_ = null;
        
        sut_ = null;
        ui_ = null;
    }
    
    
    
    @Test
    public void testScenario_NormalFlow() throws Exception {
        // *** Arrange and setup  expectations ***
        reset(loanDAO_); // Stop the specified mocks counting the interactions during setup() method
        
        final int MEMBER_ID = 1; // Corresponds to member1_/borrower_
        final int BOOK_ID = 1;   // Corresponds to book1_ 
        final int EXPECTED_LOAN_ID = loanDAO_.listLoans().size() + 1;
        
        IBook expectedLoanBook = book1_;            // Remember to change these if you change the ID above
        IMember expectedLoanBorrower = member1_;
        EBookState expectedBookState = EBookState.ON_LOAN;
        int expectedLoanRecordSize = loanDAO_.listLoans().size() + 1;
        
        // Expected loan slip details
        String expectedBookAuthor = expectedLoanBook.getAuthor();
        String expectedBookTitle = expectedLoanBook.getTitle();
        String expectedBorrowerName = expectedLoanBorrower.getFirstName() + " " + expectedLoanBorrower.getLastName();
        String expectedLoanSlip = String.format("Loan ID:  %d\nAuthor:   %s\nTitle:    %s\nBorrower: %s", 
                                                EXPECTED_LOAN_ID, expectedBookAuthor, expectedBookTitle, expectedBorrowerName);
        
        // *** Begin simulation ***
        sut_.initialise();          // User selects borrow function
        sut_.cardSwiped(MEMBER_ID); // User swipes card
        sut_.bookScanned(BOOK_ID);  // User scans a book
        sut_.scansCompleted();      // User indicated they have completed scanning books
        sut_.loansConfirmed();      // User confirms pending loan list
        
        // *** Gather post simulation data ***
        List<ILoan> loanRecord = loanDAO_.listLoans();
        int actualLoanRecordSize = loanRecord.size();
        
        List<ILoan> borrowerLoanRecord = expectedLoanBorrower.getLoans();
        IBook actualLoanBook = borrowerLoanRecord.get(0).getBook();
        IMember actualLoanBorrower = borrowerLoanRecord.get(0).getBorrower();
        EBookState actualBookState = actualLoanBook.getState();
        
        // *** Confirm post conditions ***
        // Confirm loan slip printed
        verify(printer_).print(contains(expectedLoanSlip));
        
        // Confirm loan committed/recorded and added to borrower loan record
        verify(loanDAO_, times(1)).createLoan(anyObject(), anyObject());
        assertThat(actualLoanRecordSize, equalTo(expectedLoanRecordSize));
        assertThat(actualLoanBook, equalTo(expectedLoanBook));
        assertThat(actualLoanBorrower, equalTo(expectedLoanBorrower));
        
        // Confirm the book is in the correct state
        assertThat(actualBookState, equalTo(expectedBookState));
    }
    
    
    
    @Test
    public void testScenario_AlternateFlow_5_1_BorrowingRestrictedFor_OverdueLoans() throws Exception { 
        // *** Arrange and setup  expectations ***
        reset(loanDAO_); 
        
        final int MEMBER_ID = 2; 
        int expectedLoanRecordSize = loanDAO_.listLoans().size();
        int expectedBorrowerLoanRecordSize = member2_.getLoans().size();
        
        // *** Begin simulation ***
        sut_.initialise();          // User selects borrow function
        sut_.cardSwiped(MEMBER_ID); // User swipes card
        
        // User should now be unable to progress further
        sut_.cancelled();           // User selects cancel
        
        // *** Gather post simulation data ***
        List<ILoan> loanRecord = loanDAO_.listLoans();
        int actualLoanRecordSize = loanRecord.size();
        int actualBorrowerLoanRecordSize = member2_.getLoans().size();
        
        // *** Confirm post conditions ***
        // Confirm loan not committed/recorded and not added to borrower loan record
        verify(loanDAO_, never()).createLoan(anyObject(), anyObject());
        assertThat(actualLoanRecordSize, equalTo(expectedLoanRecordSize));
        assertThat(actualBorrowerLoanRecordSize, equalTo(expectedBorrowerLoanRecordSize));
    }
    
    
    
    @Test
    public void testScenario_AlternateFlow_5_1_BorrowingRestrictedFor_MaxLoansReached() throws Exception {
        // *** Arrange and setup  expectations ***
        ILoan newLoan = loanDAO_.createLoan(member5_, book1_); // Put member5_ at the loan limit
        loanDAO_.commitLoan(newLoan);
        
        reset(loanDAO_); // Stop the specified mocks counting the interactions from the line above and during the setup() method
        
        final int MEMBER_ID = 5; 
        int expectedLoanRecordSize = loanDAO_.listLoans().size();
        int expectedBorrowerLoanRecordSize = member5_.getLoans().size();
        
        // *** Begin simulation ***
        sut_.initialise();          // User selects borrow function
        sut_.cardSwiped(MEMBER_ID); // User swipes card
        
        // User should now be unable to progress further
        sut_.cancelled();           // User selects cancel
        
        // *** Gather post simulation data ***
        List<ILoan> loanRecord = loanDAO_.listLoans();
        int actualLoanRecordSize = loanRecord.size();
        int actualBorrowerLoanRecordSize = member5_.getLoans().size();

        // *** Confirm post conditions *** 
        // Confirm loan not committed/recorded and not added to borrower loan record
        verify(loanDAO_, never()).createLoan(anyObject(), anyObject());
        assertThat(actualLoanRecordSize, equalTo(expectedLoanRecordSize));
        assertThat(actualBorrowerLoanRecordSize, equalTo(expectedBorrowerLoanRecordSize));
    }
    
    
    
    @Test
    public void testScenario_AlternateFlow_5_1_BorrowingRestrictedFor_MaxFines() throws Exception {
        // *** Arrange and setup  expectations ***
        reset(loanDAO_); 
        
        final int MEMBER_ID = 4; 
        int expectedLoanRecordSize = loanDAO_.listLoans().size();
        int expectedBorrowerLoanRecordSize = member4_.getLoans().size();
        
        // *** Begin simulation ***
        sut_.initialise();          // User selects borrow function
        sut_.cardSwiped(MEMBER_ID); // User swipes card
        
        // User should now be unable to progress further
        sut_.cancelled();           // User selects cancel
        
        // *** Gather post simulation data ***
        List<ILoan> loanRecord = loanDAO_.listLoans();
        int actualLoanRecordSize = loanRecord.size();
        int actualBorrowerLoanRecordSize = member4_.getLoans().size();

        
        // *** Confirm post conditions ***
        // Confirm loan not committed/recorded and not added to borrower loan record
        verify(loanDAO_, never()).createLoan(anyObject(), anyObject());
        assertThat(actualLoanRecordSize, equalTo(expectedLoanRecordSize));
        assertThat(actualBorrowerLoanRecordSize, equalTo(expectedBorrowerLoanRecordSize));
    }
    
    
    
    @Test
    public void testScenario_AlternateFlow_5_2_MultipleBooksBorrowed() throws Exception {
        // *** Arrange and setup  expectations ***
        reset(loanDAO_); 
        
        final int MEMBER_ID = 1; 
        final int BOOK_ID1 = 1;   
        final int BOOK_ID2 = 8;   
        final int EXPECTED_LOAN_ID1 = loanDAO_.listLoans().size() + 1;
        final int EXPECTED_LOAN_ID2 = loanDAO_.listLoans().size() + 2;
        
        IBook expectedLoanBook1 = book1_;            
        IBook expectedLoanBook2 = book8_;
        IMember expectedLoanBorrower = member1_;
        EBookState expectedBookState = EBookState.ON_LOAN;
        int expectedLoanRecordSize = loanDAO_.listLoans().size() + 2;
        
        // Expected loan slip details
        String expectedBookAuthor1 = expectedLoanBook1.getAuthor();
        String expectedBookTitle1 = expectedLoanBook1.getTitle();
        String expectedBorrowerName = expectedLoanBorrower.getFirstName() + " " + expectedLoanBorrower.getLastName();
        String expectedLoanSlipBook1 = String.format("Loan ID:  %d\nAuthor:   %s\nTitle:    %s\nBorrower: %s", 
                                                     EXPECTED_LOAN_ID1, expectedBookAuthor1, expectedBookTitle1, expectedBorrowerName);
        
        String expectedBookAuthor2 = expectedLoanBook2.getAuthor();
        String expectedBookTitle2 = expectedLoanBook2.getTitle();
        String expectedLoanSlipBook2 = String.format("Loan ID:  %d\nAuthor:   %s\nTitle:    %s\nBorrower: %s", 
                                                     EXPECTED_LOAN_ID2, expectedBookAuthor2, expectedBookTitle2, expectedBorrowerName);
        
        // *** Begin simulation ***
        sut_.initialise();          // User selects borrow function
        sut_.cardSwiped(MEMBER_ID); // User swipes card
        sut_.bookScanned(BOOK_ID1); // User scans a book
        sut_.bookScanned(BOOK_ID2); // User scans a second book
        sut_.scansCompleted();      // User indicated they have completed scanning books
        sut_.loansConfirmed();      // User confirms pending loan list
        
        // *** Gather post simulation data ***
        List<ILoan> loanRecord = loanDAO_.listLoans();
        int actualLoanRecordSize = loanRecord.size();
        
        List<ILoan> borrowerLoanRecord = expectedLoanBorrower.getLoans();
        IBook actualLoanBook1 = borrowerLoanRecord.get(0).getBook();
        IBook actualLoanBook2 = borrowerLoanRecord.get(1).getBook();
        IMember actualLoanBorrower = borrowerLoanRecord.get(0).getBorrower();
        EBookState actualBookState1 = actualLoanBook1.getState();
        EBookState actualBookState2 = actualLoanBook2.getState();
        
        // *** Confirm post conditions ***
        // Confirm loan slip printed
        verify(printer_).print(contains(expectedLoanSlipBook1));
        verify(printer_).print(contains(expectedLoanSlipBook2));
        
        // Confirm loans committed/recorded and added to borrower loan record
        verify(loanDAO_, times(2)).createLoan(anyObject(), anyObject());
        assertThat(actualLoanRecordSize, equalTo(expectedLoanRecordSize));
        assertThat(actualLoanBook1, equalTo(expectedLoanBook1));
        assertThat(actualLoanBook2, equalTo(expectedLoanBook2));
        assertThat(actualLoanBorrower, equalTo(expectedLoanBorrower));
        
        // Confirm the books are in the correct state
        assertThat(actualBookState1, equalTo(expectedBookState));
        assertThat(actualBookState2, equalTo(expectedBookState));
    }
    
    
    
    @Test
    public void testScenario_AlternateFlow_5_3_LoanLimitReached() throws Exception {
        // *** Arrange and setup  expectations ***
        reset(loanDAO_); 
        
        final int MEMBER_ID = 5; 
        final int BOOK_ID = 1;   
        final int EXPECTED_LOAN_ID = loanDAO_.listLoans().size() + 1;
        
        IBook expectedLoanBook = book1_;            
        IMember expectedLoanBorrower = member5_;
        EBookState expectedBookState = EBookState.ON_LOAN;
        int expectedLoanRecordSize = loanDAO_.listLoans().size() + 1;
        
        // Expected loan slip details
        String expectedBookAuthor = expectedLoanBook.getAuthor();
        String expectedBookTitle = expectedLoanBook.getTitle();
        String expectedBorrowerName = expectedLoanBorrower.getFirstName() + " " + expectedLoanBorrower.getLastName();
        String expectedLoanSlip = String.format("Loan ID:  %d\nAuthor:   %s\nTitle:    %s\nBorrower: %s", 
                                                EXPECTED_LOAN_ID, expectedBookAuthor, expectedBookTitle, expectedBorrowerName);
        
        // *** Begin simulation ***
        sut_.initialise();          // User selects borrow function
        sut_.cardSwiped(MEMBER_ID); // User swipes card
        sut_.bookScanned(BOOK_ID);  // User scans a book which puts them at loan limit
        
        // Scan completion panel should be shown now
        sut_.loansConfirmed();      // User confirms pending loan list
        
        // *** Gather post simulation data ***
        List<ILoan> loanRecord = loanDAO_.listLoans();
        int actualLoanRecordSize = loanRecord.size();
        
        List<ILoan> borrowerLoanRecord = expectedLoanBorrower.getLoans();
        int lastRecord = borrowerLoanRecord.size() - 1;
        IBook actualLoanBook = borrowerLoanRecord.get(lastRecord).getBook();
        IMember actualLoanBorrower = borrowerLoanRecord.get(0).getBorrower();
        EBookState actualBookState = actualLoanBook.getState();
        
        // *** Confirm post conditions ***
        // Confirm loan slip printed
        verify(printer_).print(contains(expectedLoanSlip));
        
        // Confirm loan committed/recorded and added to borrower loan record
        verify(loanDAO_, times(1)).createLoan(anyObject(), anyObject());
        assertThat(actualLoanRecordSize, equalTo(expectedLoanRecordSize));
        assertThat(actualLoanBook, equalTo(expectedLoanBook));
        assertThat(actualLoanBorrower, equalTo(expectedLoanBorrower));
        
        // Confirm the book is in the correct state
        assertThat(actualBookState, equalTo(expectedBookState));
    }
    
    
    
    @Test
    public void testScenario_AlternateFlow_5_4_RejectPendingLoans() throws Exception {
        // *** Arrange and setup  expectations ***
        reset(loanDAO_); 
        
        final int MEMBER_ID = 1; 
        final int BOOK_ID1 = 1;    
        final int BOOK_ID2 = 8;   
        final int EXPECTED_LOAN_ID1 = loanDAO_.listLoans().size() + 1;
        final int EXPECTED_LOAN_ID2 = loanDAO_.listLoans().size() + 2;
        
        IBook expectedLoanBook1 = book1_;            
        IBook expectedLoanBook2 = book8_;
        IMember expectedLoanBorrower = member1_;
        EBookState expectedBookState = EBookState.ON_LOAN;
        int expectedLoanRecordSize = loanDAO_.listLoans().size() + 2;
        
        // Expected loan slip details
        String expectedBookAuthor1 = expectedLoanBook1.getAuthor();
        String expectedBookTitle1 = expectedLoanBook1.getTitle();
        String expectedBorrowerName = expectedLoanBorrower.getFirstName() + " " + expectedLoanBorrower.getLastName();
        String expectedLoanSlipBook1 = String.format("Loan ID:  %d\nAuthor:   %s\nTitle:    %s\nBorrower: %s", 
                                                     EXPECTED_LOAN_ID1, expectedBookAuthor1, expectedBookTitle1, expectedBorrowerName);
        
        String expectedBookAuthor2 = expectedLoanBook2.getAuthor();
        String expectedBookTitle2 = expectedLoanBook2.getTitle();
        String expectedLoanSlipBook2 = String.format("Loan ID:  %d\nAuthor:   %s\nTitle:    %s\nBorrower: %s", 
                                                     EXPECTED_LOAN_ID2, expectedBookAuthor2, expectedBookTitle2, expectedBorrowerName);
        
        // *** Begin simulation ***
        sut_.initialise();          // User selects borrow function
        sut_.cardSwiped(MEMBER_ID); // User swipes card
        sut_.bookScanned(BOOK_ID1); // User scans book1_
        sut_.scansCompleted();      // User indicated they have completed scanning books
        sut_.loansRejected();       // User rejects current loan list (which should be cleared)
        sut_.bookScanned(BOOK_ID1); // User scans book1_
        sut_.bookScanned(BOOK_ID2); // User scans book2_
        sut_.scansCompleted();      // User indicated they have completed scanning books
        sut_.loansConfirmed();      // User confirms pending loan list
        
        // *** Gather post simulation data ***
        List<ILoan> loanRecord = loanDAO_.listLoans();
        int actualLoanRecordSize = loanRecord.size();
        
        List<ILoan> borrowerLoanRecord = expectedLoanBorrower.getLoans();
        IBook actualLoanBook1 = borrowerLoanRecord.get(0).getBook();
        IBook actualLoanBook2 = borrowerLoanRecord.get(1).getBook();
        IMember actualLoanBorrower = borrowerLoanRecord.get(0).getBorrower();
        EBookState actualBookState1 = actualLoanBook1.getState();
        EBookState actualBookState2 = actualLoanBook2.getState();
        
        // *** Confirm post conditions ***
        // Confirm loan slip printed
        verify(printer_).print(contains(expectedLoanSlipBook1));
        verify(printer_).print(contains(expectedLoanSlipBook2));
        
        // Confirm loans committed/recorded and added to borrower loan record
        verify(loanDAO_, times(3)).createLoan(anyObject(), anyObject());
        assertThat(actualLoanRecordSize, equalTo(expectedLoanRecordSize));
        assertThat(actualLoanBook1, equalTo(expectedLoanBook1));
        assertThat(actualLoanBook2, equalTo(expectedLoanBook2));
        assertThat(actualLoanBorrower, equalTo(expectedLoanBorrower));
        
        // Confirm the books are in the correct state
        assertThat(actualBookState1, equalTo(expectedBookState));
        assertThat(actualBookState2, equalTo(expectedBookState));
    }
}



