package library.entities;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import library.interfaces.entities.ILoan;
import library.interfaces.entities.EBookState;

// Book implementation Unit Test
public class BookImplTest {
    
    private Book sut_;
    
    @Mock
    private ILoan loan_ = mock(ILoan.class);
    
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    
    
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        
        final int BOOK_ID = 1;
        
        sut_ = new Book("Bob Bobson", "Tales of Jim Jimson", "123456789", BOOK_ID);
    }

    
    
    @After
    public void tearDown() throws Exception {
        loan_ = null;
    }

    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_NullAuthor() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
         
        new Book(null, "Tales of Jim Jimson", "0", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_NullTitle() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
         
        new Book("Bob Bobson", null, "0", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_NullCallNumber() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
         
        new Book("Bob Bobson", "Tales of Jim Jimson", null, BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_EmptyAuthor() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
        
        new Book("", "Tales of Jim Jimson", "0", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_EmptyTitle() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
        
        new Book("Bob Bobson", "", "0", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_EmptyCallNumber() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
        
        new Book("Bob Bobson", "Tales of Jim Jimson", "", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_BlankAuthor() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
        
        new Book("     ", "Tales of Jim Jimson", "0", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_BlankTitle() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
        
        new Book("Bob Bobson", "     ", "0", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_BlankCallNumber() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 1;
        
        new Book("Bob Bobson", "Tales of Jim Jimson", "        ", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }

    
    
    @Test
    public void creatingBook_ShouldThrowExceptionFor_IdZero() {
        thrown.expect(RuntimeException.class);
        final int BOOK_ID = 0;
        
        new Book("Bob Bobson", "Tales of Jim Jimson", "0", BOOK_ID);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void borrowingBook_ShouldCorrectlySetLoan() throws Exception { 
        sut_.borrow(loan_);
        
        ILoan actual = sut_.getLoan();
        
        assertThat(actual, equalTo(loan_));
    } 
    
 
    
    @Test
    public void borrowingBook_ShouldThrowExceptionFor_BookAlreadyOnLoan() {
        thrown.expect(RuntimeException.class);
        
        sut_.setState(EBookState.ON_LOAN);
        sut_.borrow(loan_);    
        
        fail("Exception should have been thrown.");
    }  
    
    
    
    @Test
    public void afterBorrowingBook_StateShouldBe_OnLoan() throws Exception { 
        sut_.borrow(loan_);
        
        EBookState actual = sut_.getState(); 
        
        assertThat(actual, equalTo(EBookState.ON_LOAN));
    } 
    
    
    
    @Test  
    public void getLoan_ShouldReturnLoan() throws Exception {
        sut_.borrow(loan_);
        
        assertThat(sut_.getLoan(), equalTo(loan_));
    }
    
    
    
    @Test
    public void getLoan_ShouldReturnNullWhen_BookNotOnLoan() throws Exception {
        // Even though the book should already be available, this just makes it explicit
        sut_.setState(EBookState.AVAILABLE);
        
        assertThat(sut_.getLoan(), equalTo(null));
    }
   
    
    
    @Test
    public void returningBook_ShouldThrowExceptionWhen_BookNotOnLoan() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBookState.AVAILABLE);
        boolean damaged = false;
        
        sut_.returnBook(damaged);
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void afterReturningBook_StateShouldBe_Damaged_When_Damaged() throws Exception { 
        sut_.setState(EBookState.ON_LOAN);
        boolean damaged = true;
        
        sut_.returnBook(damaged); 
        EBookState actual = sut_.getState(); 
        
        assertThat(actual, equalTo(EBookState.DAMAGED));
    } 
    
    
    
    @Test
    public void afterReturningBook_StateShouldBe_Available_When_NotDamaged() throws Exception { 
        sut_.setState(EBookState.ON_LOAN);
        boolean damaged = false;
        
        sut_.returnBook(damaged);
        EBookState actual = sut_.getState(); 
        
        assertThat(actual, equalTo(EBookState.AVAILABLE));
    } 
    
    
    
    @Test
    public void afterReturningBook_LoanShouldBe_Null() throws Exception { 
        sut_.setState(EBookState.ON_LOAN);
        
        sut_.returnBook(false);
        ILoan actual = sut_.getLoan(); 
        
        assertNull(actual);
    } 
    
    
    
    @Test
    public void losingBook_ShouldThrowExceptionWhen_BookNotOnLoan() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBookState.AVAILABLE);
        
        sut_.lose();
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void afterLosingBook_StateShouldBe_Lost() throws Exception { 
        sut_.setState(EBookState.ON_LOAN);
        
        sut_.lose();
        EBookState actual = sut_.getState(); 
        
        assertThat(actual, equalTo(EBookState.LOST));
    } 
    
    
    
    @Test
    public void repairingBook_ShouldThrowExceptionWhen_BookNotDamaged() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBookState.AVAILABLE);
        
        sut_.repair();
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void afterRepairingBook_StateShouldBe_Available() throws Exception {
        sut_.setState(EBookState.DAMAGED);
        
        sut_.repair();
        EBookState actual = sut_.getState();
        
        assertThat(actual, equalTo(EBookState.AVAILABLE));
    }
    
    
    
    @Test
    public void disposingOfBook_ShouldThrowExceptionWhen_BookIsOnLoan() {
        thrown.expect(RuntimeException.class);
        sut_.setState(EBookState.ON_LOAN);
        
        sut_.dispose();
        
        fail("Exception should have been thrown.");
    }
    
    
    
    @Test
    public void afterDisposingOfBook_StateShouldBe_Disposed() throws Exception {
        sut_.setState(EBookState.AVAILABLE);
        
        sut_.dispose();
        EBookState actual = sut_.getState();
        
        assertThat(actual, equalTo(EBookState.DISPOSED));
    }
    
    
    
    @Test
    public void getAuthor_ShoulReturn_Author() throws Exception {
        final int BOOK_ID = 1;
        String expectedAuthor = "Bob Bobson";
        Book book = new Book("Bob Bobson", "The Secret Life of Jim Jimson", "5932871", BOOK_ID);
        
        String actualAuthor = book.getAuthor();
        
        assertThat(actualAuthor, equalTo(expectedAuthor));
    }
    
    
    
    @Test
    public void getTitle_ShoulReturn_Title() throws Exception {
        final int BOOK_ID = 1;
        String expectedTitle = "The Secret Life of Jim Jimson";
        Book book = new Book("Bob Bobson", "The Secret Life of Jim Jimson", "5932871", BOOK_ID);
        
        String actualTitle = book.getTitle();
        
        assertThat(actualTitle, equalTo(expectedTitle));
    }
    
    
    
    @Test
    public void getCallNumber_ShoulReturn_CallNumber() throws Exception {
        final int BOOK_ID = 1;
        String expectedCallNumber = "5932871";
        Book book = new Book("Bob Bobson", "The Secret Life of Jim Jimson", "5932871", BOOK_ID);
        
        String actualCallNumber = book.getCallNumber();
        
        assertThat(actualCallNumber, equalTo(expectedCallNumber));
    }
    
    
    
    @Test
    public void getID_ShoulReturn_ID() throws Exception {
        final int BOOK_ID = 1;
        Book book = new Book("Bob Bobson", "The Secret Life of Jim Jimson", "5932871", BOOK_ID);
        
        int actualID = book.getID();
        
        assertThat(actualID, equalTo(BOOK_ID));
    }
    
    
    
    @Test
    public void toString_ShoulReturn_ValidString() throws Exception {
        final int BOOK_ID = 1;
        Book book = new Book("Bob Bobson", "The Secret Life of Jim Jimson", "5932871", BOOK_ID);
        
        String actualString = book.toString();
        
        assertThat(actualString, allOf(containsString("Bob Bobson"), containsString("The Secret Life of Jim Jimson"), containsString("5932871")));
    } 
}



